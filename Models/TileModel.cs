﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media;

namespace Mini2D_Test.Models
{
    public class TileModel : INotifyPropertyChanged
    {
        #region PROPERTIES
        /// <summary>
        /// Id of the tile
        /// </summary>
        public int id;

        /// <summary>
        /// Name of asset associated with the tile
        /// </summary>
        //public string name;

        /// <summary>
        /// Position
        /// </summary>
        public Point position = new Point(0,0);

        /// <summary>
        /// Dimensions
        /// </summary>
        public Size size = new Size { Width = 100, Height = 100 };

        /// <summary>
        /// Background Image source
        /// </summary>
        public string source = "";

        /// <summary>
        /// Type of tile
        /// </summary>
        public ObjectType type = ObjectType.Tile;

        /// <summary>
        /// Boolean is tile occupied by a tile or object
        /// </summary>
        public bool occupied = false;

        /// <summary>
        /// Color of the stroke
        /// </summary>
        public Color stroke = Colors.Blue;

        /// <summary>
        /// Tile fill
        /// </summary>
        public Brush fill = Brushes.Transparent;

        /// <summary>
        /// Thickness
        /// </summary>
        public double thickness = 0.1;

        /// <summary>
        /// Behaviors associated with this tile type
        /// </summary>
        public ObservableCollection<BehaviorModel> TileBehaviours = new ObservableCollection<BehaviorModel>();
        #endregion

        #region CONSTRUCTOR
        public TileModel() { }
        #endregion

        #region INotifyPropertyChanged Members

        /// <summary>
        /// Raises the 'PropertyChanged' event when the value of a property of the view model has changed.
        /// </summary>
        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion

        #region EVENTS
        /// <summary>
        /// 'PropertyChanged' event that is raised when the value of a property of the view model has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
    }
}
