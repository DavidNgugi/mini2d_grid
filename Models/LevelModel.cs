﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Media;

namespace Mini2D_Test.Models
{
    public class LevelModel: INotifyPropertyChanged
    {
        #region PROPERTIES

        // Strings
        public string name = "Level ";
        public string bgColor = "#333333";
        public string order = "1";

        // Integers
        public int width { get; set; }
        public int height { get; set; }
        public int twidth = 100;
        public int theight = 100;

        // Booleans
        public bool isEnabled = false;

        // Brushes
        public ImageBrush bgImage;

        // Collections
        public ObservableCollection<LevelModel> levelOrderCollection = new ObservableCollection<LevelModel>();

        /// <summary>
        /// Level Order
        /// </summary>
        public string Order {
            get { return order; }
            set
            {
                if (order == value)
                {
                    return;
                }

                order = value;

                OnPropertyChanged("order");
            }
        }

        #endregion

        #region CONSTRUCTOR
        public LevelModel()
        {
        }
        #endregion

        #region INotifyPropertyChanged Members

        /// <summary>
        /// Raises the 'PropertyChanged' event when the value of a property of the view model has changed.
        /// </summary>
        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion

        #region EVENTS
        /// <summary>
        /// 'PropertyChanged' event that is raised when the value of a property of the view model has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
    }
}
