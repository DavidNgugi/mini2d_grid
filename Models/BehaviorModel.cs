﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mini2D_Test.Models
{
    public class BehaviorModel
    {
        public int pAwardable = 0;
        public int pDeductable = 0;
        public MiniKeys moveLeftKey = MiniKeys.ArrowLeft;
        public MiniKeys moveRightKey = MiniKeys.ArrowRight;
        public MiniKeys jumpKey = MiniKeys.ArrowUp;
        public MiniKeys shootKey = MiniKeys.Spacebar;
    }
}
