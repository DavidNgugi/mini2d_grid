﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mini2D_Test.Gaming
{
    class FrameAnimation : SpriteManager
    {
        public FrameAnimation(Texture2D Texture, int frames, int animations)
            : base(Texture, frames, animations)
        {
            
        }

        public void SetFrame(int frame)
        {
            if (frame >= 0 && frame < Animations[Animation].Frames)
                FrameIndex = frame;
        }
    }
}
