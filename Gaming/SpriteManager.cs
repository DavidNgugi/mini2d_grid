﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mini2D_Test.Gaming
{
    public abstract class SpriteManager
    {
        #region PROPERTIES
        protected Texture2D Texture;
        public Vector2 Position = Vector2.Zero;
        public Color Color = Color.White;
        public Vector2 Origin;
        public float Rotation = 0f;
        public float Scale = 1f;
        public SpriteEffects SpriteEffect;

        protected Dictionary<string, AnimationClass> Animations = new Dictionary<string, AnimationClass>(); // Source rectangles
        protected int FrameIndex = 0;
        private string animation;
        public string Animation
        {
            get { return animation; }
            set { animation = value; FrameIndex = 0; }
        }

        private int Frames;
        public int width;
        public int height;
        #endregion
        
        public SpriteManager(Texture2D Texture, int frames, int animations)
        {
            this.Texture = Texture;
            this.Frames = frames;
            width = Texture.Width / Frames;
            height = Texture.Height / animations;
            Origin = new Vector2(width / 2, height / 2);
        }

        public void AddAnimation(string name, int row, int frames, AnimationClass animation)
        {
            Rectangle[] recs = new Rectangle[frames];
            for (int i = 0; i < frames; i++)
            {
                recs[i] = new Rectangle(i * width, (row - 1) * height, width, height);
            }

            animation.Frames = frames;
            animation.Rectangles = recs;
            Animations.Add(name, animation);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw
            (
                Texture, Position,
                Animations[Animation].Rectangles[FrameIndex],
                Animations[Animation].Color,
                Animations[Animation].Rotation,
                Origin,
                Animations[Animation].Scale,
                Animations[Animation].SpriteEffect, 
                0f
            );
        }
    }
}
