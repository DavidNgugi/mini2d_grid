﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mini2D_Test.Gaming
{
    class SpriteAnimation : SpriteManager
    {
        private float timeElapsed;
        public bool IsLooping = false;

        private float timeToUpdate = 0.05f;
        // Default to 30 fps
        public int FramesPerSecond
        {
            set { timeToUpdate = (1.5f / value); }
        }

        public SpriteAnimation(Texture2D Texture, int frames, int animations)
            : base(Texture, frames, animations)
        {
        
        }

        public void Update(GameTime gameTime)
        {
            timeElapsed += (float)gameTime.ElapsedGameTime.TotalSeconds;

            if(timeElapsed > timeToUpdate)
            {
                timeElapsed -= timeToUpdate;

                if (FrameIndex < Animations[Animation].Frames - 1)
                    FrameIndex = (FrameIndex + 1) % Animations[Animation].Frames;
                else if ( Animations[Animation].IsLooping)
                    FrameIndex = (FrameIndex + 1) % Animations[Animation].Frames;
            }
        }

    }
}
