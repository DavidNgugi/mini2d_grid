﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Mini2D_Test.Gaming
{
    public class AnimationClass
    {
        public Microsoft.Xna.Framework.Rectangle[] Rectangles;
        public Microsoft.Xna.Framework.Color Color = Microsoft.Xna.Framework.Color.White;
        public Vector2 Origin;
        public float Rotation = 0f;
        public float Scale = 1f;
        public SpriteEffects SpriteEffect;
        public bool IsLooping = true;
        public int Frames;

        public AnimationClass Copy()
        {
            AnimationClass ac = new AnimationClass();
            ac.Rectangles = Rectangles;
            ac.Color = Color;
            ac.Origin = Origin;
            ac.Rotation = Rotation;
            ac.Scale = Scale;
            ac.SpriteEffect = SpriteEffect;
            ac.IsLooping = IsLooping;
            ac.Frames = Frames;

            return ac;
        }
    }
}
