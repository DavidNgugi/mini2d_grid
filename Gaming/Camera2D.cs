﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mini2D_Test.Gaming
{
    class Camera2D
    {
        public Matrix transform;
        Viewport view;
        Vector2 centre;

        public Camera2D(Viewport newView)
        {
            view = newView;
        }

        public void Update(GameTime gameTime, SpriteAnimation sprite)
        {
            centre = new Vector2(sprite.Position.X + (sprite.width /2) - 200, 0);
            transform = Matrix.CreateScale(new Vector3(1,1,0)) * 
                Matrix.CreateTranslation(new Vector3(-centre.X, -centre.Y, 0));
        }
    }
}
