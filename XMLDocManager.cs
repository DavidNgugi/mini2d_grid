﻿using Mini2D_Test.ViewModels;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using System.Xml;
using System.Linq;
using System.Threading.Tasks;
using Mini2D_Test.Models;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Mini2D_Test
{
    class XMLDocManager
    {
        #region PROPERTIES
        XmlDocument doc;

        /// <summary>
        /// File variables
        /// </summary>
        private string asset_path;
        private string destination_path;
        private string project_path;
        private string project_scene_path;
        #endregion

        #region CONSTRUCTOR
        public XMLDocManager()
        {
            this.doc = new XmlDocument();

            this.asset_path = Globals.AssetPath;
            this.destination_path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments, Environment.SpecialFolderOption.Create) + @"\Mini2D\projects\";
            this.project_path = Globals.DirectoryPath + Globals.Name + ".xml";
            this.project_scene_path = Globals.DirectoryPath + Globals.Name + ".scene";

            this.LoadFile();
        }
        #endregion

        #region SETUP
        /// <summary>
        /// Load the project file
        /// </summary>
        public void LoadFile()
        {
            if (File.Exists(this.project_path))
            {
                try
                {
                    this.doc.Load(this.project_path);
                }
                catch (Exception ex)
                {
                    Debug.dd("Loading XML File", ex.Message);
                }
            }
        }
        #endregion

        #region SETUP CURRENT LEVEL
        /// <summary>
        /// Set current level after opening a project
        /// </summary>
        /// <param name="pname"></param>
        /// <returns></returns>
        public LevelViewModel SetLevel(string pname)
        {
            LevelViewModel level = new LevelViewModel();

            try
            {
                this.project_path = this.destination_path + pname + ".xml";
                this.doc.Load(this.project_path);

                XmlElement root = this.doc.DocumentElement;
                XmlNodeList elemlist = root.GetElementsByTagName("level");                

                if (elemlist != null && elemlist.Count >= 0)
                {
                    XmlNode levelNode = this.doc.SelectSingleNode("root/project/level");

                    level = new LevelViewModel
                    {
                        Width = int.Parse(levelNode.Attributes["width"].Value),
                        Height = int.Parse(levelNode.Attributes["height"].Value),
                        TileWidth = int.Parse(levelNode.Attributes["tilewidth"].Value),
                        TileHeight = int.Parse(levelNode.Attributes["tileheight"].Value)
                    };

                    return level;
                }

                return level;

            }
            catch (Exception ex)
            {
                Debug.dd("Set Level", ex.Message);
            }

            return level;
        }
        #endregion

        #region CREATE NEW PROJECT
        /// <summary>
        /// Create a New project, Check if the global project name has already been set
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public bool CreateNewProject(Project project)
        {
            string path = this.destination_path + project.Name + ".xml";

            if (string.Equals(Globals.Name, project.Name) || File.Exists(path))
            {
                return false;
            }
            else
            {
                Globals.Name = project.Name;
                Globals.Type = project.Type;
                Globals.Init();
                this.CreateNewProjectFile(project);
                return true;
            }
        }

        #endregion

        #region CREATE NEW PROJECT FILE
        /// <summary>
        /// Create a new Project XML file
        /// </summary>
        /// <param name="project"></param>
        private void CreateNewProjectFile(Project project)
        {
            try
            {
                if (!Directory.Exists(this.project_path))
                {
                    Directory.CreateDirectory(this.destination_path);
                }

                string filename = this.destination_path + project.Name + ".xml";
                Debug.dd("project filepath", filename);
                XmlTextWriter writer = new XmlTextWriter(filename, System.Text.Encoding.UTF8);
                writer.WriteStartDocument(true);
                writer.Formatting = Formatting.Indented;
                writer.Indentation = 2;
                writer.WriteStartElement("root");
                writer.WriteStartElement("project");
                writer.WriteAttributeString("xmlns", "name", null, project.Name);
                writer.WriteAttributeString("xmlns", "type", null, project.Type);
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Flush();
                writer.Close();

                Globals.DirectoryPath = this.destination_path;
            }
            catch (Exception ex)
            {
                Debug.dd("CreateNewProjectFile", ex.Message);
            }
        }
        #endregion

        #region NEW LEVEL
        /// <summary>
        /// Create a new level node
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public bool CreateNewlevel(LevelModel level)
        {
            try
            {
                if (!File.Exists(this.project_path))
                {
                    return false;
                }
                else
                {
                    this.CreateNewLevelNodes(level);
                    Debug.dd("XMLDocManager - CreateNewLevel()", level.name);
                    //Globals.Level = level;
                    //Globals.Init();
                    //Globals.HasLevels();
                    return true;
                }
            }
            catch (Exception ex) {
                Debug.dd("XMLDocManager - CreateNewLevel()", ex.Message);
                return false;
            }
        }
        #endregion

        #region CREATE NEW LEVEL NODES
        /// <summary>
        /// Create the level node as a child of Project Element and scene node as level child
        /// </summary>
        /// <param name="level"></param>
        private void CreateNewLevelNodes(LevelModel level)
        {
            try
            {
                // Define new elements 
                XmlElement levelElement = this.doc.CreateElement("level");
                XmlElement sceneElement = this.doc.CreateElement("scene");

                // Define parent nodes
                XmlNode parentNode = this.doc.SelectSingleNode("root/project");
                XmlNode levelNode = (XmlNode)levelElement;

                // For the level element
                levelElement.SetAttribute("name", level.name);
                levelElement.SetAttribute("order", level.order);
                levelElement.SetAttribute("width", level.width.ToString());
                levelElement.SetAttribute("height", level.height.ToString());
                levelElement.SetAttribute("tilewidth", level.twidth.ToString());
                levelElement.SetAttribute("tileheight", level.theight.ToString());

                // For Scene level element
                sceneElement.SetAttribute("src", "");
                sceneElement.SetAttribute("color", "#000000");

                //Append to parent nodes
                parentNode.AppendChild(levelElement);
                levelNode.AppendChild(sceneElement);

                // Update the XML file
                this.doc.Save(this.project_path);

            }
            catch (Exception ex)
            {
                Debug.dd("CreateNewLevelNodes", ex.Message);
            }

        }
        #endregion

        #region ADD OR UPDATE SCENE BACKGROUND NODE
        public void AddSceneBackgroundNode(AssetViewModel asset)
        {
            try
            {
                // Define parent nodes
                XmlNode parentNode = this.doc.SelectSingleNode("root/project/level/scene");
                if (parentNode != null)
                {

                    // Update the background Node
                    //XmlNode backgroundNode = this.doc.SelectSingleNode("root/project/level/scene/background");
                    parentNode.Attributes[0].Value = asset.Source;
                }

                // Update the XML file
                this.doc.Save(this.project_path);
            }
            catch (Exception ex)
            {
                Debug.dd("UpdateSceneBackgroundNode]: ", ex.Message);
            }
        }
        #endregion

        #region SerializeToXML
        public void SerializeToXML(IEnumerable<TileViewModel> sceneObjects, string background, string bgColor)
        {
            try
            {
                // Define parent nodes
                XmlNode parentNode = this.doc.SelectSingleNode("root/project/level");

                #region Remove all level child nodes
                foreach (XmlNode c_node in parentNode.ChildNodes) { parentNode.RemoveChild(c_node); }
                #endregion

                #region Create scene and tiles nodes
                XmlElement sceneElement = this.doc.CreateElement("scene");
                XmlElement tilesElement = this.doc.CreateElement("tiles");

                if (string.IsNullOrEmpty(background)) { sceneElement.SetAttribute("src", ""); }
                else if (string.IsNullOrEmpty(bgColor)) { sceneElement.SetAttribute("color", "#000000"); }
                else
                {
                    sceneElement.SetAttribute("src", background);
                    sceneElement.SetAttribute("color", bgColor);
                }

                parentNode.AppendChild(sceneElement);

                XmlNode sceneElementNode = (XmlNode)sceneElement;
                XmlNode tilesElementNode = (XmlNode)tilesElement;

                sceneElementNode.AppendChild(tilesElement);
                #endregion

                // TODO Add Create Player, Enemy, Collectables, Deductables and Destroyers nodes

                if (sceneObjects.Count() > 0)
                {
                    #region Add tiles
                    foreach (TileViewModel asset in sceneObjects)
                    {
                        // Define new elements  and nodes
                        XmlElement sceneObjectElement = this.doc.CreateElement("tile");
                        XmlNode sceneObjectNode = (XmlNode)sceneObjectElement;
                        // Unique identifier
                        sceneObjectElement.SetAttribute("id", asset.ID.ToString());
                        // Type of tile
                        sceneObjectElement.SetAttribute("type", asset.Type.ToString());
                        // Source of image
                        sceneObjectElement.SetAttribute("src", asset.Source);
                        //For the Position 
                        sceneObjectElement.SetAttribute("x", asset.Position.X.ToString());
                        sceneObjectElement.SetAttribute("y", asset.Position.Y.ToString());
                        //Append to <tiles> parent nodes
                        tilesElementNode.AppendChild(sceneObjectElement);
                    }
                    #endregion
                }
                
                // Update the XML file
                this.doc.Save(this.project_path);
            }
            catch (Exception ex)
            {
                Debug.dd("SerializeToXML", ex.Message);
            }
        }
        #endregion

        #region DELETE NODE
        public void DeleteNode(int index)
        {
            try
            {
                XmlNodeList ObjectNodes = this.doc.SelectNodes("root/project/level/scene/tiles/tile");
                XmlNode parentNode = this.doc.SelectSingleNode("root/project/level/scene/tiles");

                for (int i = 0; i < ObjectNodes.Count; i++)
                {
                    XmlNode node = ObjectNodes.Item(i);

                    if (Int32.Parse(node.Attributes["id"].Value) == index)
                    {
                        parentNode.RemoveChild(node);
                        this.doc.Save(this.project_path);
                        //break;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.dd("Delete Node: ", ex.Message);
            }
        }
        #endregion

        #region GET LEVEL COUNT
        /// <summary>
        /// set level count after opening a project
        /// </summary>
        /// <param name="pname"></param>
        /// <returns></returns>
        public int GetLevelCount(string pname)
        {
            int count = 0;
            this.project_path = this.destination_path + pname + ".xml";

            try
            {
                this.doc.Load(this.project_path);
                count = this.doc.SelectNodes("root/project/level").Count;
                return (count >= 0) ? count : 0;
            }
            catch (Exception ex)
            {
                Debug.dd("GetLevelCount", ex.Message);
                return 0;
            }

        }
        #endregion

        #region GET TILE COUNT
        public int GetTileCount()
        {
            return this.doc.SelectNodes("root/project/level/scene/tiles/tile").Count;
        }
        #endregion

        #region XML TO LEVEL VIEW MODEL OBJECT
        public LevelViewModel XMLToLevelViewObject()
        {
            LevelViewModel level = new LevelViewModel();

            try
            {
                XmlNode levelNode = this.doc.SelectSingleNode("root/project/level");

                if (levelNode != null)
                {
                    level = new LevelViewModel
                    {
                        Width = Int32.Parse(levelNode.Attributes["width"].Value),
                        Height = Int32.Parse(levelNode.Attributes["height"].Value),
                        TileWidth = Int32.Parse(levelNode.Attributes["tilewidth"].Value),
                        TileHeight = Int32.Parse(levelNode.Attributes["tileheight"].Value)
                    };
                    return level;
                }

            }
            catch (Exception e)
            {
                Debug.dd("XMLToLevelViewObject", e.Message);
            }
            return level;
        }
        #endregion

        #region XML TO SCENE OBJECT
        public ObservableCollection<TileViewModel> XMLToSceneObjects()
        {
            ObservableCollection<TileViewModel> sceneObjects = new ObservableCollection<TileViewModel>();

            try
            {
                XmlNode TileNodes = this.doc.SelectSingleNode("root/project/level/scene/tiles");

                XmlNodeList tiles = TileNodes.ChildNodes;

                if (tiles.Count > 0)
                {
                    // Iterate through and add to scene objects collection
                    for (int i = 0; i < tiles.Count; i++)
                    {
                        sceneObjects.Add(new TileViewModel
                        {
                            ID = int.Parse(tiles[i].Attributes["id"].Value),
                            Type = ObjectType.Tile,
                            Source = tiles[i].Attributes["src"].Value,
                            Position = new System.Windows.Point(int.Parse(tiles[i].Attributes["x"].Value), int.Parse(tiles[i].Attributes["y"].Value)),
                            Occupied = true
                        });
                    }
                }

                //Debug.dd("Loaded from XML to Scene Objects Count", sceneObjects.Count);
                return sceneObjects;
            }
            catch (Exception e)
            {
                Debug.dd("XMLtoSceneObjects", e.Message);
            }

            return sceneObjects;
        }
        #endregion

        #region XML TO TILE OBJECT
        public List<TileViewModel> XMLToTileObjects(string path)
        {
            List<TileViewModel> sceneObjects = new List<TileViewModel>();

            this.project_path = path;
            this.doc.Load(this.project_path);

            try
            {
                XmlNode TileNodes = this.doc.SelectSingleNode("root/project/level/scene/tiles");

                XmlNodeList tiles = TileNodes.ChildNodes;

                if (tiles.Count > 0)
                {
                    // Iterate through and add to scene objects collection
                    for (int i = 0; i < tiles.Count; i++)
                    {
                        sceneObjects.Add(new TileViewModel
                        {
                            Type = ObjectType.Tile,
                            Source = tiles[i].Attributes["src"].Value,
                            Position = new System.Windows.Point(int.Parse(tiles[i].Attributes["x"].Value), int.Parse(tiles[i].Attributes["y"].Value)),
                            Occupied = true
                        });
                    }
                }

                //Debug.dd("Loaded from XML to Scene Objects Count", sceneObjects.Count);
                return sceneObjects;
            }
            catch (Exception e)
            {
                Debug.dd("XMLToTileObjects", e.Message);
            }

            return sceneObjects;
        }
        #endregion

        #region SET BACKGROUND COLOR
        public void SetBackgroundColor(string p)
        {
            XmlNode parentNode = this.doc.SelectSingleNode("root/project/level/scene");

            if (this.doc.SelectSingleNode("root/project/level/scene").Attributes["color"].Value != null)
            {
                this.doc.SelectSingleNode("root/project/level/scene").Attributes["color"].Value = p;
            }
            else
            {
                // Set the source attribute
                parentNode.Attributes["src"].Value = "";
                parentNode.Attributes["color"].Value = p;
            }

            // Update the XML file
            this.doc.Save(this.project_path);
        }
        #endregion

        #region UPDATE BACKGROUND IMAGE
        public void UpdateBackgroundImage(string source)
        {
            XmlNode parentNode = this.doc.SelectSingleNode("root/project/level/scene");

            if (this.doc.SelectSingleNode("root/project/level/scene").Attributes["src"].Value != null)
            {
                this.doc.SelectSingleNode("root/project/level/scene").Attributes["src"].Value = source;
            }
            else
            {
                // Set the source attribute
                parentNode.Attributes["src"].Value = source;
            }

            // Update the XML file
            this.doc.Save(this.project_path);
        }
        #endregion

        #region GET BACKGROUND Image
        public string GetBackgroundImage()
        {
            return (this.doc.SelectSingleNode("root/project/level/scene") != null) ? this.doc.SelectSingleNode("root/project/level/scene").Attributes["src"].Value : string.Empty;
        }
        #endregion

        #region GET BACKGROUND Color
        public string GetBackgroundColor()
        {
            return (this.doc.SelectSingleNode("root/project/level/scene") != null) ? this.doc.SelectSingleNode("root/project/level/scene").Attributes["color"].Value : "#000000";
        }
        #endregion

        #region REMOVE NODE
        private void RemoveNode(XmlNode node)
        {
            XmlNodeList ObjectNodes = this.doc.SelectNodes("root/project/level/scene/object");
            node.ParentNode.RemoveChild(node);
            this.doc.Save(this.project_path);
        }
        #endregion

        #region CREATE SINGLE SCENE OBJECT NODE
        public void CreateSceneObjectNodes(TileViewModel asset)
        {
            try
            {
                // Define new elements 
                XmlElement sceneObjectElement = this.doc.CreateElement("object");
                //XmlElement sourceElement = this.doc.CreateElement("source");
                XmlElement sizeElement = this.doc.CreateElement("size");
                XmlElement positionElement = this.doc.CreateElement("position");

                // Define parent nodes
                XmlNode parentNode = this.doc.SelectSingleNode("root/project/level/scene");
                XmlNode sceneObjectNode = (XmlNode)sceneObjectElement;

                //For the Object element
                sceneObjectElement.SetAttribute("id", asset.ID.ToString());
                //sceneObjectElement.SetAttribute("name", asset.Name);
                sceneObjectElement.SetAttribute("type", asset.Type.ToString());
                sceneObjectElement.SetAttribute("src", asset.Source);

                //For the Size Element
                sizeElement.SetAttribute("width", asset.Size.Width.ToString());
                sizeElement.SetAttribute("height", asset.Size.Height.ToString());

                //For the Position Element
                positionElement.SetAttribute("x", asset.Position.X.ToString());
                positionElement.SetAttribute("y", asset.Position.Y.ToString());

                //Append to parent nodes
                parentNode.AppendChild(sceneObjectElement);
                sceneObjectNode.AppendChild(sizeElement);
                sceneObjectNode.AppendChild(positionElement);

                // Update the XML file
                this.doc.Save(this.project_path);

            }
            catch (Exception ex)
            {
                Debug.dd("AddSceneObjectNode]: ", ex.Message);
            }
        }
        #endregion

        #region CREATE MULTIPLE SCENE OBJECT NODE
        public void CreateMultipleSceneObjectNodes(ObservableCollection<TileViewModel> assets)
        {
            Debug.dd("Scene Objects Being Saved Count", assets.Count);
            try
            {
                XMLDocManager doc = new XMLDocManager();
                // Define parent nodes
                XmlNode parentNode = this.doc.SelectSingleNode("root/project/level");
                XmlElement sceneElement = this.doc.CreateElement("scene");
                // For Scene level element
                sceneElement.SetAttribute("src", this.GetBackgroundImage());
                sceneElement.SetAttribute("color", this.GetBackgroundColor());
                parentNode.AppendChild(sceneElement);
                XmlNode sceneElementNode = (XmlNode)sceneElement;

                foreach (TileViewModel asset in assets.ToList())
                {
                    // Define new elements 
                    XmlElement sceneObjectElement = this.doc.CreateElement("object");
                    //XmlElement sourceElement = this.doc.CreateElement("source");
                    XmlElement sizeElement = this.doc.CreateElement("size");
                    XmlElement positionElement = this.doc.CreateElement("position");
                    XmlNode sceneObjectNode = (XmlNode)sceneObjectElement;
                    //For the Object element
                    sceneObjectElement.SetAttribute("id", asset.ID.ToString());
                    //sceneObjectElement.SetAttribute("name", asset.Name);
                    sceneObjectElement.SetAttribute("type", asset.Type.ToString());
                    sceneObjectElement.SetAttribute("src", asset.Source);
                    //For the Size Element
                    sizeElement.SetAttribute("width", asset.Size.Width.ToString());
                    sizeElement.SetAttribute("height", asset.Size.Height.ToString());
                    //For the Position Element
                    positionElement.SetAttribute("x", asset.Position.X.ToString());
                    positionElement.SetAttribute("y", asset.Position.Y.ToString());
                    //Append to parent nodes
                    sceneElementNode.AppendChild(sceneObjectElement);
                    sceneObjectNode.AppendChild(sizeElement);
                    sceneObjectNode.AppendChild(positionElement);
                    //this.doc.Save(this.project_path);
                }

                // Update the XML file
                this.doc.Save(this.project_path);
            }
            catch (Exception ex)
            {
                Debug.dd("AddMultipleSceneObjectNode]: ", ex.Message);
            }
        }
        #endregion

    }
}
