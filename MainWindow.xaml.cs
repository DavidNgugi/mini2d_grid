﻿using MahApps.Metro.Controls;
using Mini2D_Test.ViewModels;
using Mini2D_Test.Views;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Mini2D_Test
{
    public partial class MainWindow : MetroWindow, INotifyPropertyChanged
    {
        #region PROPERTIES
        private bool clicked;
        #endregion

        #region CONSTRUCTOR
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = new MainWindowViewModel(this);
        }
        #endregion

        #region INTERNAL VARS AND METHODS

        internal string title
        {
            get { return Title; }
            set { Dispatcher.Invoke(new Action(() => { Title = value; RaisePropertyChanged("title"); })); }
        }

        #endregion

        #region TOP MENU EVENTS

        #region TOGGLE ERASER FUNCTION
        private void eraser_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            var vm = this.DataContext as MainWindowViewModel;
            vm.EraseCommand.CanExecute(null);
            vm.EraseCommand.Execute(btn);
        }
        #endregion

        #region TOGGLE EDIT MODE FUNCTION
        private void toggleEditMode_Click(object sender, RoutedEventArgs e)
        {
            var vm = this.DataContext as MainWindowViewModel;
            vm.ToggleEditModeCommand.CanExecute(null);
            vm.ToggleEditModeCommand.Execute(null);
        }
        #endregion

        #region TOGGLE DESIGN FUNCTION
        private void toggleDesignMode_Click(object sender, RoutedEventArgs e)
        {
            var vm = this.DataContext as MainWindowViewModel;
            vm.ToggleDesignModeCommand.CanExecute(null);
            vm.ToggleDesignModeCommand.Execute(null);
        }
        #endregion

        #endregion

        #region MAIN MENU

        #region UNDO
        private void undo_Click(object sender, RoutedEventArgs e)
        {

        }
        #endregion

        #region REDO
        private void redo_Click(object sender, RoutedEventArgs e)
        {

        }
        #endregion

        #region MENU VIEW FULLSCREEN
        private void viewFullscreen_Click(object sender, RoutedEventArgs e)
        {
            this.toggleColumns("full");
        }
        #endregion

        #region VIEW CODE
        private void viewCode_Click(object sender, RoutedEventArgs e)
        {

        }
        #endregion

        #region NEW LEVEL
        private void newLevel_Click(object sender, RoutedEventArgs e)
        {
            if (Globals.Init() && !Globals.HasLevels()) { this.CreateNewLevelDialog(); }
            else if (Globals.HasLevels()) { MessageBox.Show("Only one level is allowed in Mini2D v1.0.0 Beta", "Sorry", MessageBoxButton.OK, MessageBoxImage.Information); }
            else if (!Globals.Init()) { MessageBox.Show("Create or Open a Project first", "Sorry", MessageBoxButton.OK, MessageBoxImage.Information); }
        }

        private void CreateNewLevelDialog()
        {
            try
            {
                NewLevelView newlevel = new NewLevelView();
                newlevel.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "An Error occured \n " + ex.Message, MessageBoxButton.OK);
            }
        }
        #endregion

        #region ADD SOUND
        private void addSound_Click(object sender, RoutedEventArgs e)
        {

        }
        #endregion

        #region PROJECT MENU EVENTS

        #region PLAY GAME
        public void playGame_Click(object sender, RoutedEventArgs e)
        {
            using (Game game = new Game())
            {
                game.Run();
            }
        }
        #endregion

        //#region PAUSE GAME
        //private void pauseGame_Click(object sender, RoutedEventArgs e)
        //{

        //}
        //#endregion

        //#region EXIT GAME
        //private void exitGame_Click(object sender, RoutedEventArgs e)
        //{

        //}
        //#endregion

        #endregion

        #region VIEW DOCS
        private void viewDocs_Click(object sender, RoutedEventArgs e)
        {

        }
        #endregion

        #region ABOUT WINDOW
        private void about_Click(object sender, RoutedEventArgs e)
        {
            AboutBox1 aboutwin = new AboutBox1();
            aboutwin.ShowDialog();
        }
        #endregion

        #region TOGGLE FULLSCREEN VIEW
        private void toggleSidebar_Click(object sender, RoutedEventArgs e)
        {
            this.toggleColumns("full");
        }
        #endregion

        #region TOGGLE ASSETS VIEW
        private void toggleLeftSidebar_Click(object sender, RoutedEventArgs e)
        {
            this.toggleColumns("left");
        }
        #endregion

        #region TOGGLE PROPERTIES VIEW
        private void toggleProperties_Click(object sender, RoutedEventArgs e)
        {
            this.toggleColumns("right");
        }
        #endregion

        #region VIEW SETTINGS
        private void viewSettings_Click(object sender, RoutedEventArgs e)
        {
            SettingsView s = new SettingsView();
            s.ShowDialog();
        }
        #endregion

        #endregion

        #region MENU COMMANDS

        #region SAVE FILE COMMAND
        private void CommandBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var vm = this.DataContext as MainWindowViewModel;
            vm.SaveProjectCommand.CanExecute(null);
            vm.SaveProjectCommand.Execute(null);
        }
        #endregion

        #region NEW PROJECT
        private void CommandBinding_CanExecute_1(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CommandBinding_Executed_1(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                NewProjectView newproject = new NewProjectView();
                newproject.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "An Error occured \n " + ex.Message, MessageBoxButton.OK);
            }
        }
        #endregion

        #region OPEN PROJECT
        private void CommandBinding_CanExecute_2(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CommandBinding_Executed_2(object sender, ExecutedRoutedEventArgs e)
        {
            var vm = this.DataContext as MainWindowViewModel;
            vm.OpenProjectCommand.CanExecute(null);
            vm.OpenProjectCommand.Execute(null);
        }
        #endregion

        #endregion

        #region ASSET LIST MOUSE EVENTS
        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Image img = sender as Image;
            ImageBrush selected = new ImageBrush(img.Source);
            var vm = this.DataContext as MainWindowViewModel;
            vm.SelectCommand.CanExecute(null);
            vm.SelectCommand.Execute(selected);
        }
        #endregion

        #region EVENTS

        public event PropertyChangedEventHandler PropertyChanged;       

        private void RaisePropertyChanged(string p)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(p));
        }
        #endregion

        #region TOGGLE UI COLUMNS
        private void toggleColumns(string side)
        {
            MainGrid.ColumnDefinitions.Clear();
            if (!clicked)
            {
                clicked = true;
                MainGrid.ColumnDefinitions.Clear();
                ColumnDefinition c1 = new ColumnDefinition();
                ColumnDefinition c2 = new ColumnDefinition();
                ColumnDefinition c3 = new ColumnDefinition();

                switch (side)
                {
                    case "left":
                        leftScroller.Visibility = Visibility.Collapsed;
                        c1.Width = new GridLength(0, GridUnitType.Star);
                        c2.Width = new GridLength(6.5, GridUnitType.Star);
                        c3.Width = new GridLength(1.5, GridUnitType.Star);
                        break;
                    case "full":
                        c1.Width = new GridLength(0, GridUnitType.Star);
                        c2.Width = new GridLength(100, GridUnitType.Star);
                        c3.Width = new GridLength(0, GridUnitType.Star);
                        break;
                    case "right":
                        c1.Width = new GridLength(1, GridUnitType.Star);
                        c2.Width = new GridLength(6.5, GridUnitType.Star);
                        c3.Width = new GridLength(0, GridUnitType.Star);
                        break;
                }

                MainGrid.ColumnDefinitions.Add(c1);
                MainGrid.ColumnDefinitions.Add(c2);
                MainGrid.ColumnDefinitions.Add(c3);
            }
            else
            {
                clicked = false;
                MainGrid.ColumnDefinitions.Clear();
                leftScroller.Visibility = Visibility.Visible;
                ColumnDefinition c1 = new ColumnDefinition();
                ColumnDefinition c2 = new ColumnDefinition();
                ColumnDefinition c3 = new ColumnDefinition();
                c1.Width = new GridLength(1, GridUnitType.Star);
                c2.Width = new GridLength(5.5, GridUnitType.Star);
                c3.Width = new GridLength(1.5, GridUnitType.Star);
                MainGrid.ColumnDefinitions.Add(c1);
                MainGrid.ColumnDefinitions.Add(c2);
                MainGrid.ColumnDefinitions.Add(c3);
            }
        }
        #endregion

        #region KEYBOARD SHORTCUTS
        private void MainGrid_KeyDown(object sender, KeyEventArgs e)
        {
            var vm = this.DataContext as MainWindowViewModel;

            // Open Project
            if ((e.Key == Key.O) && ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control))
            {
                vm.OpenProjectCommand.CanExecute(null);
                vm.OpenProjectCommand.Execute(null);
            }
            else if ((e.Key == Key.L) && ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control))
            {
                // New Level 
                if (Globals.Init() && !Globals.HasLevels()) { this.CreateNewLevelDialog(); }
                else if (Globals.HasLevels()) { MessageBox.Show("Only one level is allowed in Mini2D v1.0.0 Beta", "Sorry", MessageBoxButton.OK, MessageBoxImage.Information); }
                else if (!Globals.Init()) { MessageBox.Show("Create or Open a Project first", "Sorry", MessageBoxButton.OK, MessageBoxImage.Information); }
            }
            else if ((e.Key == Key.P) && ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control))
            {
                // Preview game/ Play game
                using (Game game = new Game())
                {
                    game.Run();
                }
            }
            else if ((e.Key == Key.A) && ((Keyboard.Modifiers & (ModifierKeys.Control | ModifierKeys.Shift)) == (ModifierKeys.Control | ModifierKeys.Shift)))
            {
                // Add Tile Assets
                vm.AddImageAssetCommand.CanExecute(null);
                vm.AddImageAssetCommand.Execute(null);
            }
            else if ((e.Key == Key.D) && ((Keyboard.Modifiers & (ModifierKeys.Control | ModifierKeys.Shift)) == (ModifierKeys.Control | ModifierKeys.Shift)))
            {
                // Edit/Select Mode
                vm.ToggleLevelEditMode();
            }
        }
        #endregion

        #region BACKGROUND COLOR PICKER       
        private void canvasBackgroundColorPicker_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            Xceed.Wpf.Toolkit.ColorPicker p = sender as Xceed.Wpf.Toolkit.ColorPicker;
            var vm = DataContext as MainWindowViewModel;
            if (Globals.Init() && Globals.HasLevels()) 
            { 
                Color bc = (System.Windows.Media.Color)p.SelectedColor; 
                SolidColorBrush sbc = new SolidColorBrush(bc);
                // Call set bg color command
                vm.ChangeBgColorCommand.CanExecute(null);
                vm.ChangeBgColorCommand.Execute(sbc);
            }
        }
        #endregion

        private void objectType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBoxItem item = sender as ListBoxItem;
            
        }
    }
}
