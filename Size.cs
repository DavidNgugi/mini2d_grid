﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mini2D_Test
{
    public class Size
    {
        public double Width { get; set; }
        public double Height { get; set; }

    }
}
