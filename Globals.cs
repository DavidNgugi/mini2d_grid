﻿using Mini2D_Test.ViewModels;
using System.ComponentModel;
using System.Windows.Controls;

namespace Mini2D_Test
{
    static class Globals
    {
        public static Canvas Canvas { get; set; }
        public static LevelViewModel Level { get; set; }
        public static int LevelCount { get; set; }
        public static string DirectoryPath { get; set; }
        public static string AssetPath { get; set; }
        public static string Name { get; set; }
        public static string Type { get; set; }
        public static bool Saved { get; set; }
        public static bool Init() { return (string.IsNullOrEmpty(Name)) ? false : true; }
        public static bool HasLevels() { return (LevelCount > 0) ? true : false; }
        public static void Reset() { Canvas = null; Level = null; LevelCount = 0; Name = null; Type = null; Saved = false; }
    }
}
