﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.IO;
using Mini2D_Test.Gaming;
using Mini2D_Test.GamePlay;

namespace Mini2D_Test
{
    public class Game : Microsoft.Xna.Framework.Game
    {
        SpriteAnimation playerRunAnimations;

        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        //private Viewport viewport;
        private int BackBufferWidth = 800;
        private int BackBufferHeight = 640;
        private Texture2D run_texture;
        KeyboardState kbState;
        private Texture2D idle_texture;
        private SpriteAnimation playerIdleAnimations;
        Level level;

        Vector2 position;

        public Viewport viewport;
        private Camera2D camera;

        public Game()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = BackBufferWidth;
            graphics.PreferredBackBufferHeight = BackBufferHeight;

            this.IsMouseVisible = true;
            Content.RootDirectory = "Content";
            viewport = new Viewport();
            viewport.Width = 800;
            viewport.Height = 640;
            // Set the camera
            camera = new Camera2D(viewport);
            
        }

        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            try
            {
                spriteBatch = new SpriteBatch(GraphicsDevice);
                string run_filepath = @"" + Globals.AssetPath + @"\Player\Run.png";
                string idle_filepath = @"" + Globals.AssetPath + @"\Player\Idle.png";

                using (var s = File.OpenRead(run_filepath))
                {
                    run_texture = Texture2D.FromStream(GraphicsDevice, s);
                }

                using (var s = File.OpenRead(idle_filepath))
                {
                    idle_texture = Texture2D.FromStream(GraphicsDevice, s);
                }

                AnimationClass anim = new AnimationClass();

                playerIdleAnimations = new SpriteAnimation(idle_texture, 1, 1);
                playerIdleAnimations.AddAnimation("idle", 1, 1, anim.Copy());
                playerIdleAnimations.Position = new Vector2(96, 96);
                playerIdleAnimations.IsLooping = true;
                playerIdleAnimations.FramesPerSecond = 30;

                playerRunAnimations = new SpriteAnimation(run_texture, 10, 1);
                playerRunAnimations.AddAnimation("left", 1, 10, anim.Copy());

                anim.SpriteEffect = SpriteEffects.FlipHorizontally;
                playerRunAnimations.AddAnimation("right", 1, 10, anim.Copy());

                playerRunAnimations.IsLooping = false;
                playerRunAnimations.AddAnimation("idle", 0, 0, anim.Copy());
               
                playerRunAnimations.Position = new Vector2(96,96);
                playerRunAnimations.IsLooping = true;
                playerRunAnimations.FramesPerSecond = 30;

                playerIdleAnimations.Animation = "idle";
                playerRunAnimations.Animation = "idle";

                // Unloads the content for the current level before loading the next one.
                if (level != null)
                    level.Dispose();

                string path = Globals.DirectoryPath + Globals.Name + ".xml"; 
                level = new Level(graphics.GraphicsDevice, path, Globals.Level);

            }catch(Exception e)
            {
                Debug.dd("LoadContent() - Error", e.Message);
            }
        }

        protected override void UnloadContent()
        {
            // Dispose textures
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            kbState = Keyboard.GetState();

            // Move the camera when the arrow keys are pressed
            Vector2 movement = Vector2.Zero;
            Viewport vp = this.GraphicsDevice.Viewport;

            //if (kbState.IsKeyDown(Keys.Up)) { position.Y -= 5;   }
            //if (keyboard.IsKeyDown(Keys.Down)) { position.Y += 5;  }

            if (kbState.IsKeyDown(Keys.Left)) 
            {
                playerRunAnimations.Position.X -= 2;

                if (playerRunAnimations.Animation != "left")
                    playerRunAnimations.Animation = "left";
                                   
            }
            else if (kbState.IsKeyDown(Keys.Right))
            {
                playerRunAnimations.Position.X += 2;

                if (playerRunAnimations.Animation != "right")
                    playerRunAnimations.Animation = "right";
            }
            else {
                playerRunAnimations.Animation = "idle";
                playerIdleAnimations.Animation = "idle";
                playerIdleAnimations.Position = playerRunAnimations.Position;
            }

            if (playerRunAnimations.Animation == "idle")
            {
                playerIdleAnimations.Update(gameTime);
            }
            else
            {
                playerRunAnimations.Update(gameTime);
            }

            camera.Update(gameTime, playerRunAnimations);

            base.Update(gameTime);
        }

       
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.LightBlue);

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, camera.transform);

            // Draw background and tiles
            level.Draw(gameTime, spriteBatch);

            if (playerRunAnimations.Animation == "idle")
            {
                playerIdleAnimations.Draw(spriteBatch);
            }
            else
            {
                playerRunAnimations.Draw(spriteBatch);
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }

        #region DRAW HUD
        //private void DrawHud()
        //{
        //    Rectangle titleSafeArea = GraphicsDevice.Viewport.TitleSafeArea;
        //    Vector2 hudLocation = new Vector2(titleSafeArea.X, titleSafeArea.Y);
        //    Vector2 center = new Vector2(titleSafeArea.X + titleSafeArea.Width / 2.0f,
        //                                 titleSafeArea.Y + titleSafeArea.Height / 2.0f);

        //    // Draw score
        //    //DrawShadowedString(hudFont, "SCORE: " + level.Score.ToString(), hudLocation + new Vector2(0.0f, 5.0f), Color.Yellow);
        //    DrawShadowedString(hudFont, "SCORE: 0", hudLocation + new Vector2(0.0f, 5.0f), Color.Yellow);

        //    // Determine the status overlay message to show.
        //    //Texture2D status = null;
        //    //if (level.TimeRemaining == TimeSpan.Zero)
        //    //{
        //    //    if (level.ReachedExit)
        //    //    {
        //    //        status = winOverlay;
        //    //    }
        //    //    else
        //    //    {
        //    //        status = loseOverlay;
        //    //    }
        //    //}
        //    //else if (!level.Player.IsAlive)
        //    //{
        //    //    status = diedOverlay;
        //    //}

        //    //if (status != null)
        //    //{
        //    //    // Draw status message.
        //    //    Vector2 statusSize = new Vector2(status.Width, status.Height);
        //    //    spriteBatch.Draw(status, center - statusSize / 2, Color.White);
        //    //}
        //}

        //private void DrawShadowedString(SpriteFont font, string value, Vector2 position, Color color)
        //{
        //    spriteBatch.DrawString(font, value, position + new Vector2(1.0f, 1.0f), Color.Black);
        //    spriteBatch.DrawString(font, value, position, color);
        //}
        #endregion
    }
}
