﻿using Microsoft.Win32;
using Mini2D_Test.Converters;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Mini2D_Test.ViewModels
{
    class MainWindowViewModel : LevelViewModel
    {
        #region PROPERTIES
        private Window context;
        private XMLDocManager doc;
        private StringToBrushConverter sc;

        private string filename;
        private string sourcePath;
        private string sourceFile;
        private string destFile;
        private string bgImagepath = "";
        private Brush bgColor = null;

        /// <summary>
        /// Keeps track of every asset in the project including the asset information
        /// </summary>
        private ObservableCollection<AssetViewModel> assetList;
        private ObservableCollection<AssetViewModel> playerList;
        private ObservableCollection<AssetViewModel> enemyList;
        private ObservableCollection<AssetViewModel> collectablesList;
        private ObservableCollection<TileViewModel> sceneObjects;
        private ObservableCollection<int> selectedItems;

        /// <summary>
        /// Commands
        /// </summary>
        private ICommand openProjectCommand;
        private ICommand saveProjectCommand;
        private ICommand uploadBackgroundCommand;
        private ICommand changeBgColorCommand;
        private ICommand addImageAssetCommand;
        private ICommand toggleGridCommand;
        private ICommand toggleEditModeCommand;
        private ICommand toggleDesignModeCommand;
        private ICommand selectCommand;
        private ICommand eraseCommand;
        private ICommand addPlayerSpriteCommand;
        private ICommand addEnemySpriteCommand;
        private ICommand addCollectablesCommand;

        /// <summary>
        /// Level & Tile Dimensions
        /// </summary>
        public int LevelWidth { get { return base.Width; } set { base.Width = value; OnPropertyChanged("LevelWidth"); } }
        public int LevelHeight { get { return base.Height; } set { base.Height = value; OnPropertyChanged("LevelHeight"); } }
        public int LevelTileWidth { get { return base.TileWidth; } set { base.TileWidth = value; OnPropertyChanged("LevelTileWidth"); } }
        public int LevelTileHeight { get { return base.TileHeight; } set { base.TileHeight = value; OnPropertyChanged("LevelTileHeight"); } }

        public bool LevelIsEnabled { get { return base.IsEnabled; } set { base.IsEnabled = value; OnPropertyChanged("LevelIsEnabled"); } }

        /// <summary>
        /// Level Background
        /// </summary>
        public Brush LevelBackground
        {
            get
            {
                if (bgColor == null)
                {
                    return this.sc.Convert(base.BgColor);
                }
                else
                {
                    return bgColor;
                }
            }
            set
            {
                bgColor = (Brush)value;
                base.OnPropertyChanged("LevelBackground");
            }
        }

        /// <summary>
        /// Contains a collection of number of orders to be in any given project
        /// </summary>
        public ICollectionView ProjectAssets { get; private set; }

        /// <summary>
        /// Publicly accessible Assets Collection
        /// </summary>
        public ObservableCollection<AssetViewModel> Assets
        {
            get { return assetList; }
            set { assetList = value; OnPropertyChanged("Assets"); }
        }

        /// <summary>
        /// Publicly accessible Player Collection
        /// </summary>
        public ObservableCollection<AssetViewModel> Player
        {
            get { return playerList; }
            set { playerList = value; OnPropertyChanged("Player"); }
        }

        /// <summary>
        /// Publicly accessible Enemy Collection
        /// </summary>
        public ObservableCollection<AssetViewModel> Enemy
        {
            get { return enemyList; }
            set { enemyList = value; OnPropertyChanged("Enemy"); }
        }

        /// <summary>
        /// Tiles considered as Scene Objects for virtue of being assigned an asset, types and behaviors
        /// </summary>
        public ObservableCollection<TileViewModel> SceneObjects
        {
            get { return sceneObjects; }
            set { sceneObjects = value; OnPropertyChanged("SceneObjects"); }
        }

        /// <summary>
        /// Progress Bar
        /// </summary>
        private bool isProgressing = false;
        private bool gridToggled = false;
        private bool inEditMode = false;
        private bool inDesignMode = true;
        private bool isErasing = false;
        private bool isMouseDown = false;
           
        /// <summary>
        /// Brushes
        /// </summary>
        private ImageBrush selected;
        private Brush BtnEraserBg = Brushes.Transparent;
        private Brush BtnEraserFg = Brushes.White;
        private Brush BtnEditBg = Brushes.Transparent;
        private Brush BtnEditFg = Brushes.White;
        private Brush BtnDesignBg = Brushes.Transparent;
        private Brush BtnDesignFg = Brushes.White;

        private Visibility isVisible = Visibility.Collapsed;
        private TileViewModel tileSelected = new TileViewModel();

        /// <summary>
        /// Progress
        /// </summary>
        public bool IsProgressing { get { return isProgressing; } set { isProgressing = value; OnPropertyChanged("isProgressing"); } }

        public bool GridToggled { get { return gridToggled; } set { gridToggled = value; OnPropertyChanged("gridToggled"); } }

        public bool IsMouseDown { get { return isMouseDown; } set { isMouseDown = value; OnPropertyChanged("isMouseDown"); } }

        public bool InEditMode { get { return inEditMode; } set { inEditMode = value; OnPropertyChanged("inEditMode"); } }

        public bool InDesignMode { get { return inDesignMode;  } set { inDesignMode = value; OnPropertyChanged("inDesignMode"); } }

        public Visibility IsVisible { get { return isVisible; } set { isVisible = value; OnPropertyChanged("isVisible"); } }

        public TileViewModel TileSelected { get { return tileSelected; } set { tileSelected = value; OnPropertyChanged("TileSelected"); } }
        #endregion

        #region CONSTRUCTOR & SETUP
        public MainWindowViewModel()
        {
            this.Setup();
        }

        public MainWindowViewModel(Window w)
        {
            context = w;
            this.Setup();
        }

        private void Setup() 
        {
            Globals.DirectoryPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\Mini2D\projects\";
            Globals.AssetPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\Mini2D\assets\";

            this.sc = new StringToBrushConverter();
            this.sceneObjects = new ObservableCollection<TileViewModel>();
            this.tileCollection = new ObservableCollection<TileViewModel>();
            this.assetList = new ObservableCollection<AssetViewModel>();
            this.playerList = new ObservableCollection<AssetViewModel>();
            this.enemyList = new ObservableCollection<AssetViewModel>();
            this.collectablesList = new ObservableCollection<AssetViewModel>();
            this.selectedItems = new ObservableCollection<int>();

            // Set the data source
            ProjectAssets = new ListCollectionView(Assets);
            Globals.Level = this;
            this.doc = new XMLDocManager();
        }
        #endregion

        #region COMMANDS

        #region MENU COMMANDS
        /// <summary>
        /// Open a Project File Command
        /// </summary>
        public ICommand OpenProjectCommand
        {
            get
            {
                if (openProjectCommand == null)
                {
                    openProjectCommand = new DelegateCommand(param => this.OpenFileDialog());
                }

                return openProjectCommand;
            }
        }

        /// <summary>
        /// Save scene to Project File Command
        /// </summary>
        public ICommand SaveProjectCommand
        {
            get
            {
                if (saveProjectCommand == null)
                {
                    saveProjectCommand = new DelegateCommand(param => this.SaveProject());
                }

                return saveProjectCommand;
            }
        }

        /// <summary>
        /// Add Image Asset Command
        /// </summary>
        public ICommand AddImageAssetCommand
        {
            get
            {
                if (addImageAssetCommand == null)
                {
                    addImageAssetCommand = new DelegateCommand(param => this.AddImageDialog());
                }

                return addImageAssetCommand;
            }
        }

        /// <summary>
        /// Add Player Sprite Command
        /// </summary>
        public ICommand AddPlayerSpriteCommand
        {
            get
            {
                if (addPlayerSpriteCommand == null)
                {
                    addPlayerSpriteCommand = new DelegateCommand(param => this.AddPlayerSpritesDialog());
                }

                return addPlayerSpriteCommand;
            }
        }

        /// <summary>
        /// Add Enemy Sprite Command
        /// </summary>
        public ICommand AddEnemySpriteCommand
        {
            get
            {
                if (addEnemySpriteCommand == null)
                {
                    addEnemySpriteCommand = new DelegateCommand(param => this.AddEnemySpritesDialog());
                }

                return addEnemySpriteCommand;
            }
        }

        /// <summary>
        /// Add Collectables Command
        /// </summary>
        public ICommand AddCollectablesCommand
        {
            get
            {
                if (addCollectablesCommand == null)
                {
                    addCollectablesCommand = new DelegateCommand(param => this.AddCollectablesDialog());
                }

                return addCollectablesCommand;
            }
        }

        /// <summary>
        /// Toggle Grid Command
        /// </summary>
        public ICommand ToggleGridCommand
        {
            get
            {
                if (toggleGridCommand == null)
                {
                    toggleGridCommand = new DelegateCommand(param => this.ToggleGrid());
                }

                return toggleGridCommand;
            }
        }

        /// <summary>
        /// Toggle Edit Mode Command
        /// </summary>
        public ICommand ToggleEditModeCommand
        {
            get
            {
                if (toggleEditModeCommand == null)
                {
                    toggleEditModeCommand = new DelegateCommand(param => this.ToggleLevelEditMode());
                }

                return toggleEditModeCommand;
            }
        }

        /// <summary>
        /// Toggle Design Mode Command
        /// </summary>
        public ICommand ToggleDesignModeCommand
        {
            get
            {
                if (toggleDesignModeCommand == null)
                {
                    toggleDesignModeCommand = new DelegateCommand(param => this.ToggleLevelDesignMode());
                }

                return toggleDesignModeCommand;
            }
        }

        #endregion

        #region SELECT COMMAND
        public ICommand SelectCommand
        {
            get
            {
                if (selectCommand == null)
                {
                    selectCommand = new DelegateCommand(param => this.SetSelecitonBrush(param));
                }

                return selectCommand;
            }
        }

        #endregion

        #region ERASE COMMAND
        public ICommand EraseCommand
        {
            get
            {
                if (eraseCommand == null)
                {
                    eraseCommand = new DelegateCommand(param => this.EraseTileBrush(param));
                }

                return eraseCommand;
            }
        }

        #endregion

        #region UPLOAD BACKGROUND IMAGE COMMAND
        public ICommand UploadBackgroundCommand
        {
            get
            {
                if (uploadBackgroundCommand == null)
                {
                    uploadBackgroundCommand = new DelegateCommand(param => this.UploadBackground());
                }

                return uploadBackgroundCommand;
            }
        }
        #endregion

        #region CHANGE BACKGROUND COLOR COMMAND
        public ICommand ChangeBgColorCommand
        {
            get
            {
                if (changeBgColorCommand == null)
                {
                    changeBgColorCommand = new DelegateCommand(param => this.ChangeBackgroundColor(param));
                }

                return changeBgColorCommand;
            }
        }
        #endregion

        #endregion

        #region ADD IMAGE DIALOG
        private void AddImageDialog()
        {
            if (Globals.Init() && Globals.HasLevels())
            {
                try
                {
                    OpenFileDialog openFileDialog = new OpenFileDialog();
                    openFileDialog.Filter = "Image files (*.png; *.gif; *.jpeg; *.bmp)|*.png; *.gif;*.jpeg; *.bmp";
                    openFileDialog.Multiselect = true;
                    openFileDialog.InitialDirectory = Globals.AssetPath;
                    openFileDialog.Title = "Choose an Image file";

                    string finalDirPath = Globals.AssetPath + @"\" + Globals.Name + @"\Tiles";

                    if (!Directory.Exists(finalDirPath))
                    {
                        Directory.CreateDirectory(finalDirPath);
                    }

                    if (openFileDialog.ShowDialog() == true)
                    {
                        string[] filenames = openFileDialog.FileNames;
                        this.UploadImageAsset(0, filenames, finalDirPath);
                    }

                }
                catch (Exception ex)
                {
                    Debug.dd("Add New Tile", ex.Message);
                    MessageBox.Show(ex.Message, "File Openning Error", MessageBoxButton.OK);
                }
            }
            else if (!Globals.Init()) { MessageBox.Show("Create or Open a Project first", "Sorry", MessageBoxButton.OK, MessageBoxImage.Information); }
            else if (Globals.Init() && !Globals.HasLevels()) { MessageBox.Show("Create or Choose a Level first", "Sorry", MessageBoxButton.OK, MessageBoxImage.Information); }
        }
        #endregion

        #region ADD PLAYER SPRITE DIALOG
        private void AddPlayerSpritesDialog()
        {
            if (Globals.Init() && Globals.HasLevels())
            {
                try
                {
                    OpenFileDialog openFileDialog = new OpenFileDialog();
                    openFileDialog.Filter = "Image files (*.png; *.gif; *.jpeg; *.bmp)|*.png; *.gif;*.jpeg; *.bmp";
                    openFileDialog.Multiselect = true;
                    openFileDialog.InitialDirectory = Globals.AssetPath;
                    openFileDialog.Title = "Choose an Image file";

                    string finalDirPath = Globals.AssetPath + @"\" + Globals.Name + @"\Player";

                    if (!Directory.Exists(finalDirPath))
                    {
                        Directory.CreateDirectory(finalDirPath);
                    }

                    if (openFileDialog.ShowDialog() == true)
                    {
                        string[] filenames = openFileDialog.FileNames;
                        this.UploadImageAsset(1, filenames, finalDirPath);
                    }

                }
                catch (Exception ex)
                {
                    Debug.dd("Add New Player", ex.Message);
                    MessageBox.Show(ex.Message, "File Openning Error", MessageBoxButton.OK);
                }
            }
            else if (!Globals.Init()) { MessageBox.Show("Create or Open a Project first", "Sorry", MessageBoxButton.OK, MessageBoxImage.Information); }
            else if (Globals.Init() && !Globals.HasLevels()) { MessageBox.Show("Create or Choose a Level first", "Sorry", MessageBoxButton.OK, MessageBoxImage.Information); }
        }
        #endregion

        #region ADD ENEMY SPRITES DIALOG
        private void AddEnemySpritesDialog()
        {
            if (Globals.Init() && Globals.HasLevels())
            {
                try
                {
                    OpenFileDialog openFileDialog = new OpenFileDialog();
                    openFileDialog.Filter = "Image files (*.png; *.gif; *.jpeg; *.bmp)|*.png; *.gif;*.jpeg; *.bmp";
                    openFileDialog.Multiselect = true;
                    openFileDialog.InitialDirectory = Globals.AssetPath;
                    openFileDialog.Title = "Choose an Image file";

                    string finalDirPath = Globals.AssetPath + @"\" + Globals.Name + @"\Enemy";

                    if (!Directory.Exists(finalDirPath))
                    {
                        Directory.CreateDirectory(finalDirPath);
                    }

                    if (openFileDialog.ShowDialog() == true)
                    {
                        string[] filenames = openFileDialog.FileNames;
                        this.UploadImageAsset(2, filenames, finalDirPath);
                    }

                }
                catch (Exception ex)
                {
                    Debug.dd("Add New Player", ex.Message);
                    MessageBox.Show(ex.Message, "File Openning Error", MessageBoxButton.OK);
                }
            }
            else if (!Globals.Init()) { MessageBox.Show("Create or Open a Project first", "Sorry", MessageBoxButton.OK, MessageBoxImage.Information); }
            else if (Globals.Init() && !Globals.HasLevels()) { MessageBox.Show("Create or Choose a Level first", "Sorry", MessageBoxButton.OK, MessageBoxImage.Information); }
        }
        #endregion

        #region ADD COLLECTABLES DIALOG
        private void AddCollectablesDialog()
        {
            if (Globals.Init() && Globals.HasLevels())
            {
                try
                {
                    OpenFileDialog openFileDialog = new OpenFileDialog();
                    openFileDialog.Filter = "Image files (*.png; *.gif; *.jpeg; *.bmp)|*.png; *.gif;*.jpeg; *.bmp";
                    openFileDialog.Multiselect = true;
                    openFileDialog.InitialDirectory = Globals.AssetPath;
                    openFileDialog.Title = "Choose an Image file";

                    string finalDirPath = Globals.AssetPath + @"\" + Globals.Name + @"\Collectables";

                    if (!Directory.Exists(finalDirPath))
                    {
                        Directory.CreateDirectory(finalDirPath);
                    }

                    if (openFileDialog.ShowDialog() == true)
                    {
                        string[] filenames = openFileDialog.FileNames;
                        this.UploadImageAsset(3, filenames, finalDirPath);
                    }

                }
                catch (Exception ex)
                {
                    Debug.dd("Add New Player", ex.Message);
                    MessageBox.Show(ex.Message, "File Openning Error", MessageBoxButton.OK);
                }
            }
            else if (!Globals.Init()) { MessageBox.Show("Create or Open a Project first", "Sorry", MessageBoxButton.OK, MessageBoxImage.Information); }
            else if (Globals.Init() && !Globals.HasLevels()) { MessageBox.Show("Create or Choose a Level first", "Sorry", MessageBoxButton.OK, MessageBoxImage.Information); }
        }
        #endregion

        #region UPLOAD IMAGE ASSET
        private void UploadImageAsset(int type, string[] filenames, string finalDirPath)
        {
            foreach (string filename in filenames)
            {
                string extension = System.IO.Path.GetExtension(filename);
                string targetImagePath = this.GetTargetPath(System.IO.Path.GetFileNameWithoutExtension(filename), extension);

                AssetViewModel t = new AssetViewModel { ID = assetList.Count, Name = System.IO.Path.GetFileNameWithoutExtension(filename), Type = 0 };
                
                if (!assetList.Contains(t))
                {
                    sourcePath = System.IO.Path.GetFullPath(filename);
                    sourceFile = System.IO.Path.Combine(sourcePath, filename);
                    destFile = System.IO.Path.Combine(finalDirPath, filename);

                    string filepath = finalDirPath + @"\" + System.IO.Path.GetFileName(filename);

                    System.Drawing.Bitmap bm = System.Drawing.Image.FromFile(filename) as System.Drawing.Bitmap;

                    #region Upload by extension - BMP, JPEG, PNG, GIF
                    switch (extension)
                    {
                        case ".bmp":
                            bm.Save(filepath, ImageFormat.Bmp);
                            break;
                        case ".jpg":
                            bm.Save(filepath, ImageFormat.Jpeg);
                            break;
                        case ".png":
                            bm.Save(filepath, ImageFormat.Png);
                            break;
                        case ".gif":
                            bm.Save(filepath, ImageFormat.Gif);
                            break;
                    }
                    #endregion

                    // Set Size and Position Objects
                    Size s = new Size { Width = bm.Width, Height = bm.Height };
                    System.Windows.Point p = new System.Windows.Point { X = 0, Y = 0 };
                    t.Size = s;
                    t.Position = p;
                    t.Source = filepath;

                    // Add to assets collection for access in UI
                    assetList.Add(t);

                    if (type == 1)
                    {
                        playerList.Add(new AssetViewModel
                        {
                            ID = playerList.Count,
                            Name = System.IO.Path.GetFileNameWithoutExtension(filename),
                            Size = s,
                            Position = p,
                            Source = filepath,
                        });
                    }
                    else if (type == 2)
                    {
                        enemyList.Add(new AssetViewModel
                        {
                            ID = enemyList.Count,
                            Name = System.IO.Path.GetFileNameWithoutExtension(filename),
                            Size = s,
                            Position = p,
                            Source = filepath,
                        });
                    }
                    else if (type == 3)
                    {
                        collectablesList.Add(new AssetViewModel
                        {
                            ID = collectablesList.Count,
                            Name = System.IO.Path.GetFileNameWithoutExtension(filename),
                            Size = s,
                            Position = p,
                            Source = filepath,
                        });
                    }

                    bm.Dispose();

                    MessageBox.Show("Asset uploaded!", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("A similarly named asset has already been uploaded!", "Reminder", MessageBoxButton.OK);
                }
            }
        }
        #endregion

        #region GET TARGET PATH
        private string GetTargetPath(string filename, string extension)
        {
            return @"C:\Users\User\Documents\Mini2D\assets\" + filename + extension;
        }
        #endregion

        #region UPLOAD BACKGROUND
        public void UploadBackground() 
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "Image files (*.png; *.gif; *.jpeg; *.bmp)|*.png; *.gif;*.jpeg; *.bmp";
                openFileDialog.Multiselect = false;
                openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
                openFileDialog.Title = "Choose an Image file";

                string finalDirPath = Globals.AssetPath + @"" + Globals.Name;

                if (!Directory.Exists(finalDirPath))
                {
                    Directory.CreateDirectory(finalDirPath);
                }

                if (openFileDialog.ShowDialog() == true)
                {
                    filename = openFileDialog.FileName;
                    this.UploadBackgroundImageAsset(filename, finalDirPath);
                }

            }
            catch (Exception ex)
            {
                Debug.dd("Add New Image", ex.Message);
                MessageBox.Show(ex.Message, "File Openning Error", MessageBoxButton.OK);
            }
        }
        #endregion

        #region UPLOAD BACKGROUND IMAGE ASSET
        /// <summary>
        /// Upload Background Image Asset
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="finalDirPath"></param>
        private void UploadBackgroundImageAsset(string filename, string finalDirPath)
        {
            string extension = System.IO.Path.GetExtension(filename);
            string targetImagePath = this.GetTargetPath(System.IO.Path.GetFileNameWithoutExtension(filename), extension);
            sourcePath = System.IO.Path.GetFullPath(filename);
            sourceFile = System.IO.Path.Combine(sourcePath, filename);
            destFile = System.IO.Path.Combine(finalDirPath, filename);

            string filepath = finalDirPath + @"\" + System.IO.Path.GetFileName(filename);
            this.doc = new XMLDocManager();

            if (!File.Exists(filepath))
            {
                AssetViewModel t = new AssetViewModel { ID = assetList.Count, Name = System.IO.Path.GetFileNameWithoutExtension(filename), Type = 0 };

                if (!assetList.Contains(t))
                {
                    System.Drawing.Bitmap bm = System.Drawing.Image.FromFile(filename) as System.Drawing.Bitmap;

                    // Upload by extension
                    switch (extension)
                    {
                        case ".bmp":
                            bm.Save(filepath, ImageFormat.Bmp);
                            break;
                        case ".jpg":
                            bm.Save(filepath, ImageFormat.Jpeg);
                            break;
                        case ".png":
                            bm.Save(filepath, ImageFormat.Png);
                            break;
                        case ".gif":
                            bm.Save(filepath, ImageFormat.Gif);
                            break;
                    }

                    // Set Size and Position Objects
                    Size s = new Size { Width = bm.Width, Height = bm.Height };
                    Point p = new Point { X = 0, Y = 0 };
                    t.Size = s;
                    t.Position = p;
                    t.Source = filepath;

                    // Update Project XML file

                    this.doc.AddSceneBackgroundNode(t);

                    // Add to assets collection for access in UI
                    assetList.Add(t);

                    LevelBackground = new ImageBrush(new BitmapImage(new Uri(filepath)));

                    bm.Dispose();

                    MessageBox.Show("background Changed!", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            else {
                this.doc.UpdateBackgroundImage(filepath);
                LevelBackground = new ImageBrush(new BitmapImage(new Uri(filepath)));
                MessageBox.Show("Background Updated!", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        #endregion 

        #region CHANGE BACKGROUND COLOR
        public void ChangeBackgroundColor(object param) 
        {
            SolidColorBrush c = param as SolidColorBrush;
            LevelBackground = c;
            XMLDocManager doc = new XMLDocManager();
            doc.SetBackgroundColor(c.ToString());
        }
        #endregion

        #region OPEN FILE DIALOG
        /// <summary>
        /// Opens a dialog for loadng a previous project file
        /// </summary>
        public void OpenFileDialog()
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "Mini2D Project files (*.xml)|*.xml";
                openFileDialog.Multiselect = false;
                openFileDialog.InitialDirectory = Globals.DirectoryPath;
                openFileDialog.Title = "Choose a project file";

                if (openFileDialog.ShowDialog() == true)
                {
                    isProgressing = true; isVisible = Visibility.Visible; OnPropertyChanged("IsVisible");

                    filename = openFileDialog.FileName;
                    string pname = System.IO.Path.GetFileNameWithoutExtension(filename);
                    Globals.AssetPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments, Environment.SpecialFolderOption.Create) + @"\Mini2D\assets\" + pname;
                    string directoryPath = Globals.AssetPath;

                    Globals.Reset();
                    Globals.Name = pname;

                    this.doc = new XMLDocManager();
                    Globals.Level = this.doc.SetLevel(pname);
                    Globals.LevelCount = this.doc.GetLevelCount(pname);

                    LevelIsEnabled = true;
                    LevelWidth = Globals.Level.Width;
                    LevelHeight = Globals.Level.Height;
                    LevelTileWidth = Globals.Level.TileWidth;
                    LevelTileHeight = Globals.Level.TileHeight;
                    this.bgImagepath = this.doc.GetBackgroundImage();
                    
                    // Iterate through the directory and find the assets
                    this.ReadFilesFromFolder(directoryPath);

                    string filepath = this.doc.GetBackgroundImage();
                    if (string.IsNullOrEmpty(filepath)) { LevelBackground = this.sc.Convert(this.doc.GetBackgroundColor()); }
                    else { LevelBackground = new ImageBrush(new BitmapImage(new Uri(filepath))); }

                    this.sceneObjects = this.doc.XMLToSceneObjects();

                    if (sceneObjects.Count > 0)
                    {
                        Debug.dd("Open File - Build Existing Scene", sceneObjects.Count);

                        this.BuildExistingScene(this.sceneObjects, Globals.Level.Width, Globals.Level.Height, Globals.Level.TileWidth, Globals.Level.TileHeight);
                    }
                    else
                    {
                        Debug.dd("Open File - Build New Scene", sceneObjects.Count);
                        this.BuildNewScene(Globals.Level.Width, Globals.Level.Height, Globals.Level.TileWidth, Globals.Level.TileHeight);
                    }

                    IsProgressing = false; IsVisible = Visibility.Collapsed;

                    openFileDialog = null;
                    MessageBox.Show("Project " + pname + " loaded");
                }
            }
            catch (OperationCanceledException opex)
            {
                Debug.dd("Open Project Dialog", opex.Message);
            }
            catch (Exception ex)
            {
                Debug.dd("Open Project Dialog", ex.Message);
                MessageBox.Show(ex.Message, " [Open Project Dialog] File Openning Error", MessageBoxButton.OK);
            }

        }
        #endregion

        #region READ FILES FROM FOLDER
        /// <summary>
        /// Task Read files from folder and build assets list
        /// </summary>
        /// <returns></returns>
        public void ReadFilesFromFolder(string directoryPath)
        {
            this.ClearAssetLists();
           
            try
            {
                if (Directory.Exists(directoryPath))
                {
                    string TilesDir = directoryPath + @"\Tiles";
                    string PlayerDir = directoryPath + @"\Player";
                    string EnemyDir = directoryPath + @"\Enemy";
                    string CollectablesDir = directoryPath + @"\Collectables";

                    IEnumerable<FileInfo> info_all = this.GetFileInfo(directoryPath);

                    foreach (FileInfo f in info_all)
                    {
                        Assets.Add(new AssetViewModel { Name = System.IO.Path.GetFileNameWithoutExtension(f.Name), Source = directoryPath + @"\" + f.Name });
                    }

                    if (Directory.Exists(TilesDir))
                    {
                        IEnumerable<FileInfo> tiles_all = this.GetFileInfo(TilesDir);

                        foreach (FileInfo f in tiles_all)
                        {
                            Assets.Add(new AssetViewModel { Name = System.IO.Path.GetFileNameWithoutExtension(f.Name), Source = TilesDir + @"\" + f.Name });
                        }
                    }

                    if (Directory.Exists(PlayerDir))
                    {
                        IEnumerable<FileInfo> player_all = this.GetFileInfo(PlayerDir);

                        foreach (FileInfo f in player_all)
                        {
                            //Assets.Add(new AssetViewModel { Name = System.IO.Path.GetFileNameWithoutExtension(f.Name), Source = PlayerDir + @"\" + f.Name });
                            playerList.Add(new AssetViewModel { Name = System.IO.Path.GetFileNameWithoutExtension(f.Name), Source = PlayerDir+ @"\" + f.Name });
                        
                        }
                    }

                    if (Directory.Exists(EnemyDir))
                    {
                        IEnumerable<FileInfo> enemy_all = this.GetFileInfo(EnemyDir);

                        foreach (FileInfo f in enemy_all)
                        {
                            //Assets.Add(new AssetViewModel { Name = System.IO.Path.GetFileNameWithoutExtension(f.Name), Source = EnemyDir + @"\" + f.Name });
                            enemyList.Add(new AssetViewModel { Name = System.IO.Path.GetFileNameWithoutExtension(f.Name), Source = EnemyDir + @"\" + f.Name });
                        }
                    }

                    if (Directory.Exists(CollectablesDir))
                    {
                        IEnumerable<FileInfo> collectables_all = this.GetFileInfo(CollectablesDir);

                        foreach (FileInfo f in collectables_all)
                        {
                            Assets.Add(new AssetViewModel { Name = System.IO.Path.GetFileNameWithoutExtension(f.Name), Source = CollectablesDir + @"\" + f.Name });
                            collectablesList.Add(new AssetViewModel { Name = System.IO.Path.GetFileNameWithoutExtension(f.Name), Source = CollectablesDir + @"\" + f.Name });
                        }
                    }

                }
                else
                {
                    Assets = new ObservableCollection<AssetViewModel>();
                }

            }
            catch (Exception ex)
            {
                Debug.dd("ReadFilesFromFolder - Exception", ex.Message);
                Assets = new ObservableCollection<AssetViewModel>();
            }
        }
        #endregion

        #region GET FILE INFO
        public IEnumerable<FileInfo> GetFileInfo(string directoryPath) 
        {
            DirectoryInfo dirInfo = new DirectoryInfo(directoryPath);

            IEnumerable<FileInfo> bmp_info = dirInfo.EnumerateFiles("*.bmp", SearchOption.TopDirectoryOnly);
            IEnumerable<FileInfo> png_info = dirInfo.EnumerateFiles("*.png", SearchOption.TopDirectoryOnly);
            IEnumerable<FileInfo> jpg_info = dirInfo.EnumerateFiles("*.jpg", SearchOption.TopDirectoryOnly);
            IEnumerable<FileInfo> gif_info = dirInfo.EnumerateFiles("*.gif", SearchOption.TopDirectoryOnly);

            IEnumerable<FileInfo> info_bmp_png = (IEnumerable<FileInfo>)bmp_info.Concat<FileInfo>(png_info).ToArray();
            IEnumerable<FileInfo> info_jpg_gif = (IEnumerable<FileInfo>)jpg_info.Concat<FileInfo>(gif_info).ToArray();

            IEnumerable<FileInfo> info_all = (IEnumerable<FileInfo>)info_bmp_png.Concat<FileInfo>(info_jpg_gif).ToArray();
            
            return info_all;

        }
        #endregion

        #region SAVE PROJECT
        public void SaveProject() 
        {
            IEnumerable<TileViewModel> occupiedTiles =  tileCollection.ToList().Where(r => r.Occupied == true);
          
            // TODO Get player sprite

                //<player x = "" y ="" width = "" height = "">
                //    <idle src ="" row="" frames = "" width="" height=""/>           // When just standing around
                //    <walkrun src ="" row="" frames = "" width="" height=""/>        // When running/walking
                //    <jump src ="" row="" frames = "" width="" height=""/>           // When jumping Up
                //    <falling src ="" row=""  frames = ""width="" height=""/>        // When Falling down
                //    <shoot src =""row=""  frames = ""width="" height=""/>           // When shooting a gun or whatever
                //    <die src ="" row="" frames = "" width="" height=""/>            // When player dies
                //    <win src ="" row="" frames = "" width="" height=""/>            // When player wins
                //    <attack src ="" row="" frames = "" width="" height=""/>         // When a player attacks an enemy
                //    <attacked src ="" row="" frames = "" width="" height=""/>       // When a player is attacked
                //</player>

            // TODO Get Enemy Sprites list. Enemy attacks and reduces your score

                //<enemy id = "" points = "" x = "" y ="" width = "" height = ""> // Id to uniquely identify an enemy. They can be multiple
                //    <idle src ="" row="" frames = "" width="" height=""/>           // When just standing around
                //    <walkrun src ="" row="" frames = "" width="" height=""/>        // When running/walking
                //    <jump src ="" row="" frames = "" width="" height=""/>           // When jumping Up
                //    <falling src ="" row=""  frames = ""width="" height=""/>        // When falling down
                //    <shoot src =""row=""  frames = ""width="" height=""/>           // When shooting a gun or whatever
                //    <die src ="" row="" frames = "" width="" height=""/>            // When enemy is killed
                //    <win src ="" row="" frames = "" width="" height=""/>            // When enemy wins a fight/duel 
                //    <attack src ="" row="" frames = "" width="" height=""/>         // When an enemy attacks a player
                //    <attacked src ="" row="" frames = "" width="" height=""/>       // When an enemy is attacked
                //</enemy>

            // TODO Add collectables such as coins etc. that increase player score

                //<collectables>
                //     <collectable id = "" points = "" src = "" position = "x,y">
                //         <animations>
                //            <idle id = "" src ="" row="" frames = "" width="" height=""/>           // First animation, just spinning around
                //            <picked id = "" src ="" row="" frames = "" width="" height=""/>         // When picked up by player
                //         </animations>
                //    </collectable>
                //</collectables>

            // TODO Add deductables such as bombs etc. that reduce player score

                //<deductables>
                //     <deductable id = "" points = "" src = "" position = "x,y">
                //         <animations>
                //            <idle id = "" src ="" row="" frames = "" width="" height=""/>           // First animation, just spinning around
                //            <picked id = "" src ="" row="" frames = "" width="" height=""/>         // When picked up by player
                //         </animations>
                //    </deductable>
                //</deductables>
            try
            {
                string bg = doc.GetBackgroundImage();
                string bgcolor = doc.GetBackgroundColor();
                this.doc.SerializeToXML(occupiedTiles, bg, bgcolor);
                MessageBox.Show("Project Saved");
            }
            catch (Exception e)
            {
                Debug.dd("Save Project", e.Message);
            }
            
        }
        #endregion

        #region CLEAR ALL LIST
        /// <summary>
        /// Clears the Assets collection
        /// </summary>
        private void ClearAssetLists()
        {
            this.assetList.Clear();
            this.playerList.Clear();
            this.enemyList.Clear();
            this.collectablesList.Clear();
        }
        #endregion

        #region BUILD EXISTING SCENE
        public void BuildExistingScene()
        {
            
        }
        #endregion

        #region TOGGLE GRID
        public void ToggleGrid()
        {
            if (gridToggled)
            {
                this.tileCollection.ToList().ForEach(r => { r.Thickness = 0.1; r.Stroke = Colors.LightGray; });
                gridToggled = false;
            }
            else
            {
                this.tileCollection.ToList().ForEach(r => { r.Thickness = 0; r.Stroke = Colors.Transparent; });
                gridToggled = true;
            }
        }
        #endregion

        #region TILE MOUSE COMMANDS EXECUTION
        /// <summary>
        /// Handles Tile Mouse Enter
        /// </summary>
        /// <param name="param"></param>
        public void TileMouseEnter(object sender, MouseEventArgs e)
        {
            Rectangle t = sender as Rectangle;

            int id = int.Parse(t.Uid);
            var el = tileCollection.ElementAt(id);

            if (id != tileSelected.ID)
            {
                el.Stroke = Colors.White;
                el.Thickness = (gridToggled) ? 0.1 : 0.5;
            }

            if (inDesignMode)
            {
                this.IsInDesignMode(id, t, el);
            }
        }

        /// <summary>
        /// Handles Tile Mouse Left Button Down
        /// </summary>
        /// <param name="param"></param>
        public void TileMouseBtnDown(object sender, MouseEventArgs e)
        {
           isMouseDown = true;

           Rectangle t = (sender as Rectangle);
           ImageBrush b = new ImageBrush();

           int id = int.Parse(t.Uid);
           var el = tileCollection.ElementAt(id);

           Debug.dd("Mouse Down Tile ID", el.ID);

           if (inDesignMode)
           {
               if (isErasing)
               {
                   //b.ImageSource = null;
                   //t.Fill = b.Clone();
                   t.Fill = null;
                   el.Occupied = false;
                   el.Source = null;
               }
               else if (!isErasing && (selected != null))
               {
                   //b.ImageSource = selected.ImageSource;
                   //t.Fill = b.Clone(); 
                   t.Fill = selected;
                   el.Occupied = true;
                   el.Source = selected.ImageSource.ToString();
               }
           }
           else if (inEditMode)
           {
               // Change stroke of previous selected tile
               if (selectedItems.Count > 0) { int last_id = selectedItems.Last(); tileCollection.ElementAt(last_id).Stroke = (gridToggled) ? Colors.Transparent : Colors.LightGray; }

               // Set this tile as the current object being edited
               TileSelected = tileCollection.ElementAt(id);
               selectedItems.Add(id);

               el.Thickness = 0.5;
               el.Stroke = Colors.DarkBlue;
           }
        }

        /// <summary>
        /// Handle Design mode functions
        /// </summary>
        /// <param name="id"></param>
        /// <param name="t"></param>
        /// <param name="el"></param>
        public void IsInDesignMode(int id, Rectangle t, TileViewModel el)
        {
            if (isErasing && isMouseDown)
            {
                t.Fill = Brushes.Transparent;
                el.Occupied = false;
                el.Source = null;
            }
            else if (!isErasing && isMouseDown && (selected != null))
            {
                t.Fill = selected;
                el.Occupied = true;
                el.Source = selected.ImageSource.ToString();
            }
        }

        /// <summary>
        /// Handles Tile Mouse Left button Up
        /// </summary>
        /// <param name="param"></param>
        public void TileMouseBtnUp(object sender, MouseEventArgs e)
        {
            isMouseDown = false;
        }

        /// <summary>
        /// Handles Tile Mouse Leave
        /// </summary>
        /// <param name="param"></param>
        public void TileMouseLeave(object sender, MouseEventArgs e)
        {
            Rectangle t = (sender as Rectangle);
            int id = int.Parse(t.Uid);
            var el = tileCollection.ElementAt(id);

            if (id != tileSelected.ID)
            {
                el.Stroke = (gridToggled) ? Colors.Transparent : Colors.LightGray;
                el.Thickness = (gridToggled) ? 0 : 0.1;
            }

        }
        #endregion

        #region SET TILE SELECTION BRUSH
        public void SetSelecitonBrush(object param)
        {
            this.selected = param as ImageBrush;
        }
        #endregion

        #region ERASE TILE BRUSH
        public void EraseTileBrush(object param) 
        {
            Button b = param as Button;
            TextBlock tb1 = new TextBlock();
            TextBlock tb2 = new TextBlock();
            tb1.Text = "Eraser"; tb2.Text = "Erasing";
            isErasing = (isErasing) ? false : true;
            b.Content = (isErasing) ? tb2 : tb1;
            b.Background = (isErasing) ? Brushes.Blue : BtnEraserBg;
            b.Foreground = (isErasing) ? Brushes.White : BtnEraserFg;
        }
        #endregion

        #region TOGGLE LEVEL EDIT MODE
        public void ToggleLevelEditMode() 
        {
            InDesignMode = (InEditMode) ? true : false;
            InEditMode = (InEditMode) ? false : true;
        }
        #endregion

        #region TOGGLE LEVEL DESIGN MODE
        public void ToggleLevelDesignMode()
        {
            InEditMode = (InDesignMode) ? true : false;
            InDesignMode = (InDesignMode) ? false : true;
        }
        #endregion

        #region IS TILE OCCUPIED
        private bool IsOccupied(int id)
        { 
            var el = tileCollection.ElementAt(id);
            if (el != null)
                return (el.Occupied) ? true : false;
            else
                return false;
        }
        #endregion
    }
}
