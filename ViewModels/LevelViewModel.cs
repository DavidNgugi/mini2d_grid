﻿using Mini2D_Test.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
//using System.Reflection;

namespace Mini2D_Test.ViewModels
{
    class LevelViewModel : ViewModelBase, INotifyCollectionChanged, IDataErrorInfo
    {
        #region PROPERTIES

        public LevelModel lm;

        XMLDocManager doc;

        Window currentWindow;

        /// <summary>
        /// Commands
        /// </summary>
        DelegateCommand createNewLevelCommand;

        /// <summary>
        /// IDataErrorInfo properties
        /// </summary>
        public string error;

        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string columnName]
        {
            get
            {
                error = null;

                if (columnName == "levelName")
                {
                    error = this.ValidateLevelName();
                }
                else if (columnName == "levelWidth" || columnName == "levelHeight")
                {
                    error = this.ValidateLevelDimensions();
                }
                else if (columnName == "tileWidth" || columnName == "tileHeight")
                {
                    error = this.ValidateTileDimensions();
                }
                else
                {
                    //error = (lm as IDataErrorInfo)[columnName];
                    CommandManager.InvalidateRequerySuggested();
                    return error;
                }

                return null;

            }
        }

        public double x;
        public double y;
        public ObservableCollection<TileViewModel> tileCollection;

        #endregion

        #region CONSTRUCTOR & SETUP
        public LevelViewModel()
        {
            this.Setup();
        }

        public LevelViewModel(Window w)
        {
            currentWindow = w;
            this.Setup();
        }

        public void Setup()
        {
            lm = new LevelModel();
            tileCollection = new ObservableCollection<TileViewModel>();
            // Add 3 levels
            lm.levelOrderCollection.Add(new LevelModel { Order = "1" });
            lm.levelOrderCollection.Add(new LevelModel { Order = "2" });
            lm.levelOrderCollection.Add(new LevelModel { Order = "3" });
            // Set the data source
            LevelOrders = new ListCollectionView(lm.levelOrderCollection);
            // On any change to the list
            LevelOrders.MoveCurrentToFirst();
            LevelOrders.CurrentChanged += LevelOrders_CurrentChanged;
        }
        #endregion

        #region SELECTED ITEM IN LIST CHANGED
        private void LevelOrders_CurrentChanged(object sender, EventArgs e)
        {
            lm.Order = LevelOrders.CurrentItem as string;
        }
        #endregion

        #region PUBLIC PROPERTIES
        /// <summary>
        /// Contains a collection of number of orders to be in any given project
        /// </summary>
        public ICollectionView LevelOrders { get; private set; }

        /// <summary>
        /// Level Name
        /// </summary>
        public string Name
        {
            get
            {
                return lm.name;
            }
            set
            {
                if (lm.name == value)
                {
                    return;
                }

                lm.name = value;

                OnPropertyChanged("Name");
            }
        }

        /// <summary>
        /// Level Order
        /// </summary>
        public string Order
        {
            get
            {
                return lm.order;
            }
            set
            {
                if (lm.order == value)
                {
                    return;
                }

                lm.order = value;

                OnPropertyChanged("Order");
            }
        }

        /// <summary>
        /// Level Width
        /// </summary>
        public int Width
        {
            get
            {
                return lm.width;
            }
            set
            {
                if (lm.width == value)
                {
                    return;
                }

                lm.width = value;

                OnPropertyChanged("width");
            }
        }

        /// <summary>
        /// Level Height
        /// </summary>
        public int Height
        {
            get
            {
                return lm.height;
            }
            set
            {
                if (lm.height == value)
                {
                    return;
                }

                lm.height = value;

                OnPropertyChanged("height");
            }
        }

        /// <summary>
        /// Level Tile Width
        /// </summary>
        public int TileWidth
        {
            get
            {
                return lm.twidth;
            }
            set
            {
                if (lm.twidth == value)
                {
                    return;
                }

                lm.twidth = value;

                OnPropertyChanged("TileWidth");
            }
        }

        /// <summary>
        /// Level Tile Height
        /// </summary>
        public int TileHeight
        {
            get
            {
                return lm.theight;
            }
            set
            {
                if (lm.theight == value)
                {
                    return;
                }

                lm.theight = value;

                OnPropertyChanged("TileHeight");
            }
        }

        /// <summary>
        /// Boolean value for whether level is enabled
        /// </summary>
        public bool IsEnabled
        {
            get { return lm.isEnabled; }
            set
            {
                if (lm.isEnabled = value)
                {
                    return;
                }
                lm.isEnabled = value;
                OnPropertyChanged("IsEnabled");
            }
        }

        /// <summary>
        /// Level Background Image
        /// </summary>
        public ImageBrush BgImage
        {
            get
            {
                return lm.bgImage;
            }
            set
            {
                if (lm.bgImage == value)
                {
                    return;
                }

                lm.bgImage = value;

                OnPropertyChanged("bgImage");
            }
        }

        /// <summary>
        /// Level Background Color
        /// </summary>
        public string BgColor
        {
            get
            {
                return lm.bgColor;
            }
            set
            {
                if (lm.bgColor == value)
                {
                    return;
                }

                lm.bgColor = value;

                OnPropertyChanged("bgColor");
            }
        }

        /// <summary>
        /// Create New Level Command
        /// </summary>
        public ICommand CreateNewLevelCommand
        {
            get
            {
                if (createNewLevelCommand == null)
                {
                    createNewLevelCommand = new DelegateCommand(param => this.CreateNewLevel());
                }

                return createNewLevelCommand;
            }
        }

        /// <summary>
        /// Publicly accessible Tiles Collection
        /// </summary>
        public ObservableCollection<TileViewModel> TileCollection
        {
            get { return tileCollection; }
            set { tileCollection = value; OnPropertyChanged("TileCollection"); }
        }
        #endregion

        #region CREATE LEVEL
        public void CreateNewLevel()
        {
            this.doc = new XMLDocManager();

            try
            {
                if (string.IsNullOrEmpty(error) && !string.IsNullOrEmpty(lm.name))
                {
                    if (this.doc.CreateNewlevel(lm))
                    {
                        //IsEnabled = true;
                        //BuildNewScene(this.Width, this.Height, this.TileWidth, this.TileHeight);
                        MessageBox.Show("Created a New level successfully!!");
                        if (currentWindow != null) { currentWindow.Close(); }
                    }
                    else
                    {
                        MessageBox.Show("A problem occured!");
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.dd("Create New Level Click", ex.Message);
                MessageBox.Show("A problem occured while creating a new level! " + ex.Message);
            }
        }
        #endregion

        #region EVENTS
        /// <summary>
        /// 'CollectionChamged' event is raised
        /// </summary>
        public event NotifyCollectionChangedEventHandler CollectionChanged;
        #endregion

        #region DATA VALIDATION METHODS
        /// <summary>
        /// Validate Level Dimensions
        /// </summary>
        /// <returns></returns>
        private string ValidateLevelDimensions()
        {
            if (Width <= 500 || Height <= 500)
            {
                return "Width or Height of the level cannot be less than 500px each";
            }

            return null;
        }

        /// <summary>
        /// Validate Tile Dimensions
        /// </summary>
        /// <returns></returns>
        private string ValidateTileDimensions()
        {
            if (TileWidth < 32 || TileHeight < 32)
            {
                return "Tile Width or Height of the level cannot be less than 32px each";
            }
            else if (TileWidth > 128 || TileHeight < 128)
            {
                return "Tile Width or Height of the level cannot be greater than 128px each";
            }

            return null;
        }

        /// <summary>
        /// Validate Level name
        /// </summary>
        /// <returns></returns>
        private string ValidateLevelName()
        {
            if (string.IsNullOrEmpty(this.Name) || string.IsNullOrWhiteSpace(this.Name))
            {
                return "Level name must be provided";
            }
            else
            {
                return null;
            }

        }
        #endregion

        #region BUILD EMPTY SCENE
        public void BuildNewScene(int W, int H, int tW, int tH)
        {
            tileCollection.Clear();

            try
            {
                int i = 0;
                for (x = 0; x < W; x += tW)
                {
                    for (y = 0; y < H; y += tH)
                    {
                        tileCollection.Add
                        (
                            new TileViewModel
                            {
                                ID = i,
                                Position = new Point { X = x, Y = y },
                                Size = new Size { Width = tW, Height = tH },
                                Stroke = Colors.LightGray,
                                Thickness = 0.1
                            }
                         );
                        i++;
                    }
                }

                Debug.dd("BuildNewScene()", tileCollection.Count);

            }
            catch (Exception ex)
            {
                Debug.dd("BuildNewScene()", ex.Message);
            }

        }
        #endregion

        #region BUILD EXISTING SCENE
        public void BuildExistingScene(ObservableCollection<TileViewModel> sceneObjects, int W, int H, int tW, int tH)
        {
            tileCollection.Clear();

            try
            {
                foreach (TileViewModel t in sceneObjects)
                {
                    int i = 0;
                    for (x = 0; x < W; x += tW)
                    {
                        for (y = 0; y < H; y += tH)
                        {

                            if ((t.Position.X == x) && (t.Position.Y == y))
                            {
                                tileCollection.Add
                                (
                                    new TileViewModel
                                    {
                                        ID = i,
                                        Position = t.Position,
                                        Size = new Size { Width = tW, Height = tH },
                                        Stroke = Colors.LightGray,
                                        Thickness = 0.1,
                                        Source = t.Source.Replace("file:///", ""),
                                        Occupied = true
                                    }
                                 );
                            }
                            else
                            {
                                tileCollection.Add
                                (
                                    new TileViewModel
                                    {
                                        ID = i,
                                        Position = new Point { X = x, Y = y },
                                        Size = new Size { Width = tW, Height = tH },
                                        Stroke = Colors.LightGray,
                                        Thickness = 0.1,
                                        Occupied = false
                                    }
                                 );
                            }
                            i++;
                        }
                    }
                }
             
            }
            catch (Exception ex)
            {
                Debug.dd("BuildNewScene()", ex.Message);
            }

        }
        #endregion
    }
}
