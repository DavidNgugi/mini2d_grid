﻿using Mini2D_Test.Models;
using System.Windows;
using System.Windows.Media;

namespace Mini2D_Test.ViewModels
{
    class TileViewModel : ViewModelBase
    {
        #region LOCAL PROPERTIES

        /// <summary>
        /// Tile Model object
        /// </summary>
        TileModel tm;

        #endregion LOCAL PROPERTIES

        #region CONSTRUCTOR
        /// <summary>
        /// Constructor
        /// </summary>
        public TileViewModel() 
        {
            tm = new TileModel(); 
        }
        #endregion CONSTRUCTOR

        #region PUBLIC PROPERTIES
        /// <summary>
        /// The color of the stroke.
        /// </summary>
        public Color Stroke
        {
            get
            {
                return tm.stroke;
            }
            set
            {
                if (tm.stroke == value)
                {
                    return;
                }

                tm.stroke = value;

                OnPropertyChanged("Stroke");
            }
        }

        /// <summary>
        /// Id of the tile
        /// </summary>
        public int ID
        {
            get
            {
                return tm.id;
            }
            set
            {
                if (tm.id == value)
                {
                    return;
                }

                tm.id = value;

                OnPropertyChanged("ID");
            }
        }

        /// <summary>
        /// Is tile occupied by a tile asset/collectable/enemy/player
        /// </summary>
        public bool Occupied
        {
            get
            {
                return tm.occupied;
            }
            set
            {
                if (tm.occupied == value)
                {
                    return;
                }

                tm.occupied = value;

                OnPropertyChanged("Occupied");
            }
        }

        /// <summary>
        /// Name of asset associated with the tile
        /// </summary>
        
        //public string Name
        //{
        //    get
        //    {
        //        return tm.name;
        //    }
        //    set
        //    {
        //        if (tm.name == value)
        //        {
        //            return;
        //        }

        //        tm.name = value;

        //        OnPropertyChanged("Name");
        //    }
        //}

        /// <summary>
        /// Source of the 
        /// </summary>
        public string Source
        {
            get
            {
                return tm.source;
            }
            set
            {
                if (tm.source == value)
                {
                    return;
                }

                tm.source = value;

                OnPropertyChanged("Source");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ObjectType Type
        {
            get
            {
                return tm.type;
            }
            set
            {
                if (tm.type == value)
                {
                    return;
                }

                tm.type = value;

                OnPropertyChanged("Type");
            }
        }

        /// <summary>
        /// Size of the tile
        /// </summary>
        public Size Size
        {
            get
            {
                return tm.size;
            }
            set
            {
                if (tm.size == value)
                {
                    return;
                }

                tm.size = value;

                OnPropertyChanged("Size");
            }
        }

        /// <summary>
        /// Position of the tile in the scene
        /// </summary>
        public Point Position
        {
            get
            {
                return tm.position;
            }
            set
            {
                if (tm.position == value)
                {
                    return;
                }

                tm.position = value;

                OnPropertyChanged("Position");
            }
        }

        /// <summary>
        /// Tile fill
        /// </summary>
        public Brush Fill 
        {
            get
            {
                return tm.fill;
            }
            set
            {
                if (tm.fill == value)
                {
                    return;
                }

                tm.fill = value;

                OnPropertyChanged("Fill");
            }
        }

        /// <summary>
        /// Thickness
        /// </summary>
        public double Thickness 
        {
            get
            {
                return tm.thickness;
            }
            set
            {
                if (tm.thickness == value)
                {
                    return;
                }

                tm.thickness = value;

                OnPropertyChanged("Thickness");
            }
        }

        #endregion PUBLIC PROPERTIES
    }
}
