﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Mini2D_Test.ViewModels
{
    class AssetViewModel : ViewModelBase
    {
        public int id;
        public string name;
        public int type;
        public string source;
        public Point position;
        public Size size;

        public AssetViewModel() { }

        public int ID
        {
            get
            {
                return id;
            }
            set
            {
                if (id == value)
                {
                    return;
                }

                id = value;

                OnPropertyChanged("id");
            }
        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (name == value)
                {
                    return;
                }

                name = value;

                OnPropertyChanged("name");
            }
        }

        public string Source
        {
            get
            {
                return source;
            }
            set
            {
                if (source == value)
                {
                    return;
                }

                source = value;

                OnPropertyChanged("source");
            }
        }

        public int Type
        {
            get
            {
                return type;
            }
            set
            {
                if (type == value)
                {
                    return;
                }

                type = value;

                OnPropertyChanged("type");
            }
        }

        public Size Size
        {
            get
            {
                return size;
            }
            set
            {
                if (size == value)
                {
                    return;
                }

                size = value;

                OnPropertyChanged("size");
            }
        }

        public Point Position
        {
            get
            {
                return position;
            }
            set
            {
                if (position == value)
                {
                    return;
                }

                position = value;

                OnPropertyChanged("position");
            }
        }
     
    }
}
