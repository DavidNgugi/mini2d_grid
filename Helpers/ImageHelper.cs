﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Mini2D_Test.Helpers
{
    public static class ImageHelper
    {
        #region SET IMAGE BRUSH
        public static ImageBrush setImageBrush(string uri)
        {
            try
            {
                BitmapImage bitmap = new BitmapImage();
                ImageBrush imgbrush = new ImageBrush();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(uri);
                bitmap.EndInit();
                imgbrush.ImageSource = bitmap;
                return imgbrush;
            }catch(Exception e)
            {
                Debug.dd("setImageBrush()", e.Message);
                return null;
            }
            
        }
        #endregion

        #region SET IMAGE SOURCE
        public static ImageSource setImageSource(string path)
        {
            BitmapImage bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.UriSource = new Uri(path);
            bitmap.EndInit();

            return bitmap;
        }
        #endregion

        #region SET BACKGROUND COLOR BRUSH
        public static Brush setColorBrush(string p)
        {
            // TODO Make string to Brush converter here
            return Brushes.Black;
        }
        #endregion
    }
}
