﻿using Mini2D_Test.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Mini2D_Test.Converters
{
    class StringToImageSourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {

            return (!string.IsNullOrEmpty((string)value)) ? ImageHelper.setImageSource((string)value) : null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            ImageSource source =  value as ImageSource;
            ImageSource t = source.CloneCurrentValue();
           
            return t.ToString();
        }
    }
}
