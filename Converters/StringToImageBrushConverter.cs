﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Mini2D_Test.Converters
{
    class StringToImageBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (!string.IsNullOrEmpty((string)value) || ((string)value != null)) ? this.SetImageBrush((string)value) : null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException("StringToBrushCOnverter is a OneWay Converter");
        }

        public ImageBrush SetImageBrush(string uri)
        {
            BitmapImage bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.UriSource = new Uri(uri);
            bitmap.EndInit();

            ImageBrush imgbrush = new ImageBrush();
            imgbrush.ImageSource = bitmap.Clone();
            return imgbrush.Clone();
        }
    }
}
