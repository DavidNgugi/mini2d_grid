﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace Mini2D_Test.Converters
{
    class StringToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Brush b = Brushes.Transparent;
            return (!string.IsNullOrEmpty((string)value)) ? new BrushConverter().ConvertFromString((string)value) as ImageBrush : b;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException("StringToBrushCOnverter is a OneWay Converter");
        }

        public SolidColorBrush Convert(string colorString) 
        {
            return new BrushConverter().ConvertFromString(colorString) as SolidColorBrush;
        }
    }
}
