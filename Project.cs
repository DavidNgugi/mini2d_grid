﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mini2D_Test
{
    class Project
    {
        public string Name { get; set; }
        public string Type { get; set; }

        public Project(string name, string type)
        {
            Name = name;
            Type = type;
        }

    }
}
