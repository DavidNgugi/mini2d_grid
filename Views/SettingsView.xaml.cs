﻿using MahApps.Metro.Controls;
using Microsoft.Win32;
using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace Mini2D_Test.Views
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class SettingsView : MetroWindow
    {
        private string filename;
        private string pType;

        public SettingsView()
        {
            InitializeComponent();
            projectName.Text = Globals.Name;
            pType = Globals.Type;
            projectPath.Text = Globals.DirectoryPath;
            projectAssetPath.Text = Globals.AssetPath;
        }

        private void project_type_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox item = sender as ListBox;
            pType = ((ListBoxItem)item.SelectedItem).Name.ToString();
        }

        private void btnSaveSettings_Click(object sender, RoutedEventArgs e)
        {
            Globals.Name = projectName.Text;
            Globals.Type = pType;
            Globals.DirectoryPath = projectPath.Text;
            Globals.AssetPath = projectAssetPath.Text;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "Mini2D Project files (*.xml)|*.xml";
                openFileDialog.Multiselect = false;
                string fpath = (Globals.Init()) ? Globals.DirectoryPath : Directory.GetParent(@"../../Mini2D/").FullName + @"\projects";
                openFileDialog.InitialDirectory = fpath;
                openFileDialog.Title = "Choose a project file";
                if (openFileDialog.ShowDialog() == true)
                {
                    filename = openFileDialog.FileName;                   

                    //save path
                    projectPath.Text = System.IO.Path.GetFullPath(filename);
                  
                }
            }
            catch (Exception ex)
            {
                Debug.dd("Get Dir path Dialog", ex.Message);
                MessageBox.Show(ex.Message, " [Directory Path Dialog] File Openning Error", MessageBoxButton.OK);
            }
        }

        private void getAssetPath_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "Asset files (*.png; *.gif; *.jpeg; *.bmp)|*.png; *.gif;*.jpeg; *.bmp; *.mp3; *.ogg; *.wav";
                openFileDialog.Multiselect = false;
                string fpath = (Globals.Init()) ? Globals.AssetPath : Directory.GetParent(@"../../Mini2D/").FullName + @"\assets\" + Globals.Name;
                openFileDialog.InitialDirectory = fpath;
                openFileDialog.Title = "Choose an Assets directory";
                if (openFileDialog.ShowDialog() == true)
                {
                    string filename = openFileDialog.FileName;

                    // save path
                    projectAssetPath.Text = System.IO.Path.GetFullPath(filename);

                }
            }
            catch (Exception ex)
            {
                Debug.dd("Get Asset Dir path Dialog", ex.Message);
                MessageBox.Show(ex.Message, " [Asset Directory Path Dialog] File Openning Error", MessageBoxButton.OK);
            }
        }
        
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

    }
}
