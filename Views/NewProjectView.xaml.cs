﻿using MahApps.Metro.Controls;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace Mini2D_Test.Views
{
    /// <summary>
    /// Interaction logic for NewProject.xaml
    /// </summary>
    public partial class NewProjectView : MetroWindow
    {
        #region PROPERTIES
        Project project;
        public string pName;
        public string pType;
        #endregion

        public NewProjectView()
        {
            InitializeComponent();
            this.Closing += NewProjectView_Closing;
        }

        private void btnCreateProject_Click_1(object sender, RoutedEventArgs e)
        {
            XMLDocManager xmld = new XMLDocManager();

            if (String.IsNullOrEmpty(projectName.Text) || String.IsNullOrWhiteSpace(projectName.Text))
            {
                MessageBox.Show("Provide a valid project name!");
            }
            else
            {
                this.pName = projectName.Text;
                this.pType = ((ListBoxItem)project_type.SelectedItem).Name.ToString();

                project = new Project(this.pName, this.pType);

                // Create the Project XML file
                if (xmld.CreateNewProject(project))
                {
                    //MainWindow.main.title = project.Name + " - Mini2D for Windows desktop";
                    MessageBox.Show("New Project Created successfully! Proceed to create a new level");
                    NewLevelView nl = new NewLevelView();
                    nl.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("A similar Project already exists!");
                }
            }

        }

        private void project_type_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            ListBox item = sender as ListBox;
            this.pType = ((ListBoxItem)item.SelectedItem).Name.ToString();
        }

        private void Window_Closed_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.Close();
        }

        private void btnCancel_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void NewProjectView_Closing(object sender, CancelEventArgs e)
        {
           //
        }
    }

}
