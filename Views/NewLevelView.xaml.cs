﻿using MahApps.Metro.Controls;
using Mini2D_Test.ViewModels;
using System;
using System.Windows;
using System.Windows.Controls;

namespace Mini2D_Test.Views
{
    /// <summary>
    /// Interaction logic for NewLevel.xaml
    /// </summary>
    public partial class NewLevelView : MetroWindow
    {
        public NewLevelView()
        {
            InitializeComponent();
            this.DataContext = new LevelViewModel(this);
            this.Closing += NewLevelView_Closing;
        }

        private void btnCancel_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnCreateLevel_Click(object sender, RoutedEventArgs e)
        {
            var lvm = this.DataContext as LevelViewModel;
            XMLDocManager doc = new XMLDocManager();

            try
            {
                if (string.IsNullOrEmpty(lvm.error) && !string.IsNullOrEmpty(lvm.Name))
                {
                    if (doc.CreateNewlevel(lvm.lm))
                    {
                        lvm.IsEnabled = true;
                        lvm.BuildNewScene(int.Parse(this.levelWidth.Text), int.Parse(this.levelHeight.Text), int.Parse(this.tileWidth.Text), int.Parse(this.tileHeight.Text));
                        MessageBox.Show("Created a New level successfully!!");
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("A problem occured!");
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.dd("btnCreateLevel_Click()", ex.Message);
                MessageBox.Show("A problem occured while creating a new level! " + ex.Message);
            }
        }

        private void NewLevelView_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
        }
    }
}
