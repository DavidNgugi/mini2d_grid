﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mini2D_Test
{
    public enum ObjectType
    {
        Tile = 0,
        Player = 1,
        Enemy = 2,
        Collectable = 3,
        Deductable = 4,
        Destroyer = 5       
    }
}
