﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Mini2D_Test.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mini2D_Test.GamePlay
{
    class Level : IDisposable
    {
        #region PROPERTIES

        // Physical structure of the level.
        private Tile[,] tiles;
        private Texture2D[] layers;

        // Entities in the level.
        Player player;
        public Player Player
        {
            get { return player; }
        }

        private List<Collectable> collectables = new List<Collectable>();
        private List<Enemy> enemies = new List<Enemy>();

        // Key locations in the level.        
        private Vector2 start;
        private Point exit = InvalidPosition;
        private static readonly Point InvalidPosition = new Point(-1, -1);

        int score;
        public int Score
        {
            get { return score; }
        }

        bool reachedExit;
        public bool ReachedExit
        {
            get { return reachedExit; }
        }

        private int width;
        /// <summary>
        /// Width of level measured in tiles.
        /// </summary>
        public int Width
        {
            get { return tiles.GetLength(0); }
        }
        
        private int height;
        /// <summary>
        /// Height of the level measured in tiles.
        /// </summary>
        public int Height
        {
            get { return tiles.GetLength(1); }
        }

        XMLDocManager doc;
        List<TileViewModel> sceneObjects;

        Texture2D texture;
        private int EntityLayer = 1;

        GraphicsDevice graphicsDevice;
        private Texture2D background_texture;
        private int tWidth;
        private int tHeight;
        private Vector2 tileSize;
        #endregion

        #region CONSTRUCTOR
        public Level(GraphicsDevice graphicsDevice, string path, LevelViewModel lvm)
        {
            Debug.dd("Level Dimensions", lvm.Width, lvm.Height);
            Debug.dd("Level Tile Dimensions", lvm.TileWidth, lvm.TileHeight);

            this.doc = new XMLDocManager();
            this.graphicsDevice = graphicsDevice;
            this.width = lvm.Width;
            this.height = lvm.Height;
            this.tWidth = lvm.TileWidth;
            this.tHeight = lvm.TileHeight;
            this.tileSize = new Vector2(tWidth, tHeight);
            this.sceneObjects = new List<TileViewModel>();

            LoadTiles(path);

            //layers = new Texture2D[2]; // Set number of background layers

            string bg_path = this.doc.GetBackgroundImage();

            using (var s = File.OpenRead(bg_path))
            {
                background_texture = Texture2D.FromStream(graphicsDevice, s);
            }

            //for (int i = 0; i < layers.Length; ++i)
            //{
            //    // Choose a random segment if each background layer for level variety.
            //    layers[i] = background_texture;
            //}

        }
        #endregion

        /// <summary>
        /// Load the tiles from XML file
        /// </summary>
        /// <param name="path"></param>
        private void LoadTiles(string path)
        {
            try
            {
                this.sceneObjects = this.doc.XMLToTileObjects(path);

                // Allocate the tile grid.
                tiles = new Tile[this.width, this.height];

                if (sceneObjects.Count > 0)
                {
                    foreach (TileViewModel t in this.sceneObjects)
                    {
                        //// Loop over every tile position,
                        for (int y = 0; y < this.Height; y++)
                        {
                            for (int x = 0; x < this.Width; x++)
                            {
                                if ((t.Position.X == x) && (t.Position.Y == y))
                                {
                                    ////to load each tile.
                                    tiles[x, y] = LoadTile(x, y, t.Source.Replace("file:///", ""), TileCollision.Impassable);
                                }
                            }
                        }
                    }
                }

                // Verify that the level has a beginning and an end.
                //if (Player == null)
                //    throw new NotSupportedException("A level must have a starting point.");
                //if (exit == InvalidPosition)
                //    throw new NotSupportedException("A level must have an exit.");
            }
            catch (Exception ex)
            {
                Debug.dd("Load Tiles", ex.Message);
            }
        }

        /// <summary>
        /// Creates a new tile. The other tile loading methods typically chain to this
        /// method after performing their special logic.
        /// </summary>
        /// <param name="name">
        /// Path to a tile texture relative to the Content/Tiles directory.
        /// </param>
        /// <param name="collision">
        /// The tile collision type for the new tile.
        /// </param>
        /// <returns>The new tile.</returns>
        private Tile LoadTile(int x, int y, string path, TileCollision collision)
        {
            using (var s = File.OpenRead(path))
            {
                texture = Texture2D.FromStream(graphicsDevice, s);
            }

            return new Tile(texture, new Vector2(x,y), collision);
        }

        /// <summary>
        /// Gets the collision mode of the tile at a particular location.
        /// This method handles tiles outside of the levels boundries by making it
        /// impossible to escape past the left or right edges, but allowing things
        /// to jump beyond the top of the level and fall off the bottom.
        /// </summary>
        public TileCollision GetCollision(int x, int y)
        {
            // Prevent escaping past the level ends.
            if (x < 0 || x >= Width)
                return TileCollision.Impassable;
            // Allow jumping past the level top and falling through the bottom.
            if (y < 0 || y >= Height)
                return TileCollision.Passable;

            return tiles[x, y].Collision;
        }

        /// <summary>
        /// Gets the bounding rectangle of a tile in world space.
        /// </summary>        
        public Rectangle GetBounds(int x, int y)
        {
            return new Rectangle(x * Tile.Width, y * Tile.Height, Tile.Width, Tile.Height);
        }

        /// <summary>
        /// Unloads the level content.
        /// </summary>
        public void Dispose()
        {
            background_texture.Dispose();
            texture.Dispose();
        }

        /// <summary>
        /// Updates all objects in the world, performs collision between them,
        /// and handles the scoring.
        /// </summary>
        public void Update(GameTime gameTime)
        {

        }

        /// <summary>
        /// Draw everything in the level from background to foreground.
        /// </summary>
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            // if you have multiple background layers
            //for (int i = 0; i <= EntityLayer; ++i)
            //   spriteBatch.Draw(layers[i], Vector2.Zero, Color.White);

            spriteBatch.Draw(background_texture, Vector2.Zero, Color.White);
            DrawTiles(spriteBatch);
        }

        /// <summary>
        /// Draws each tile in the level.
        /// </summary>
        private void DrawTiles(SpriteBatch spriteBatch)
        {
            try
            {
                foreach (Tile t in tiles)
                {
                    Texture2D texture = t.Texture;
                    if (texture != null)
                    {
                        spriteBatch.Draw(texture, t.Position, Color.White);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.dd("Draw Tiles", ex.Message);
            }

        }

    }
}
