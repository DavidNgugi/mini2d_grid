﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Mini2D_Test.Gaming;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mini2D_Test.GamePlay
{
    class Player
    {
        #region PROPERTIES

        /// <summary>
        /// Sprite Animations
        /// </summary>

        AnimationClass anim;
        private SpriteAnimation playerRunAnimations;
        private SpriteAnimation playerIdleAnimations;
        private SpriteAnimation playerJumpAnimations;
        private SpriteAnimation playerCelebrateAnimations;
        private SpriteAnimation playerDieAnimations;
        private SpriteEffects flip = SpriteEffects.None;
        SpriteManager sprite;

        /// <summary>
        /// Texture2D tectures
        /// </summary>
        Texture2D idle_texture;
        Texture2D run_texture;
        Texture2D jump_texture;
        Texture2D celebrate_texture;
        Texture2D die_texture;

        /// <summary>
        /// Graphics Device Config
        /// </summary>
        GraphicsDevice graphicsDevice;

        /// <summary>
        /// String - Path to Assets
        /// </summary>
        string filepath;

        /// <summary>
        /// Level - Current Level
        /// </summary>
        Level level;
        public Level Level
        {
            get { return level; }
        }
        
        /// <summary>
        /// Boolean - Is Player Alive?
        /// </summary>
        bool isAlive;
        public bool IsAlive
        {
            get { return isAlive; }
        }
       
        /// <summary>
        /// Vector2 - Player Position in the world
        /// </summary>
        Vector2 position;
        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

        /// <summary>
        /// Vector2 - Velocity player is moving with
        /// </summary>
        Vector2 velocity;
        public Vector2 Velocity
        {
            get { return velocity; }
            set { velocity = value; }
        }
        
        // Constants for controling horizontal movement
        private const float MoveAcceleration = 14000.0f;
        private const float MaxMoveSpeed = 2000.0f;
        private const float GroundDragFactor = 0.58f;
        private const float AirDragFactor = 0.65f;

        // Constants for controlling vertical movement
        private const float MaxJumpTime = 0.35f;
        private const float JumpLaunchVelocity = -4000.0f;
        private const float GravityAcceleration = 3500.0f;
        private const float MaxFallSpeed = 600.0f;
        private const float JumpControlPower = 0.14f;

        // Input configuration
        private const float MoveStickScale = 1.0f;
        private const Buttons JumpButton = Buttons.A;

        /// <summary>
        /// Gets whether or not the player's feet are on the ground.
        /// </summary>
        bool isOnGround;
        public bool IsOnGround
        {
            get { return isOnGround; }
        }
        
        /// <summary>
        /// Current user movement input.
        /// </summary>
        private float movement;

        // Jumping state - isJumping, wasJumping and jumpTime
        private bool isJumping;
        private bool wasJumping;
        private float jumpTime;

        /// <summary>
        /// Gets a rectangle which bounds this player in world space.
        /// </summary>
        private Rectangle localBounds;
        public Rectangle BoundingRectangle
        {
            get
            {
                int left = (int)Math.Round(Position.X - sprite.Origin.X) + localBounds.X;
                int top = (int)Math.Round(Position.Y - sprite.Origin.Y) + localBounds.Y;

                return new Rectangle(left, top, localBounds.Width, localBounds.Height);
            }
        }

        #endregion

        #region CONSTRUCTOR
        /// <summary>
        /// Constructs a new player.
        /// </summary>
        public Player(GraphicsDevice graphicsDevice, Level level, string filepath, Vector2 position)
        {
            this.graphicsDevice = graphicsDevice;
            this.level = level;
            this.filepath = filepath;
            this.Position = position;
            
            LoadContent();

            Reset(position);
        }
        #endregion

        #region LOAD CONTENT
        /// <summary>
        /// Load player spritesheets
        /// </summary>
        private void LoadContent()
        {
            try
            {
                this.anim = new AnimationClass();

                #region FILE PATHS TO PLAYER TEXTURES
                string run_filepath = this.filepath + @"\Player\Run.png";
                string idle_filepath = this.filepath + @"\Player\Idle.png";
                string celebrate_filepath = this.filepath + @"\Player\Celebrate.png";
                string jump_filepath = this.filepath + @"\Player\Jump.png";
                string die_filepath = this.filepath + @"\Player\Die.png";
                #endregion

                #region LOAD OUR TEXTURES FROM FILE
                using (var s = File.OpenRead(run_filepath))
                {
                    run_texture = Texture2D.FromStream(graphicsDevice, s);
                }

                using (var s = File.OpenRead(idle_filepath))
                {
                    idle_texture = Texture2D.FromStream(graphicsDevice, s);
                }

                using (var s = File.OpenRead(celebrate_filepath))
                {
                    celebrate_texture = Texture2D.FromStream(graphicsDevice, s);
                }

                using (var s = File.OpenRead(jump_filepath))
                {
                    jump_texture = Texture2D.FromStream(graphicsDevice, s);
                }

                using (var s = File.OpenRead(die_filepath))
                {
                    die_texture = Texture2D.FromStream(graphicsDevice, s);
                }
                #endregion

                #region IDLE ANIMATIONS
                playerIdleAnimations = new SpriteAnimation(idle_texture, 1, 1);
                playerIdleAnimations.AddAnimation("idle", 1, 1, anim.Copy());
                playerIdleAnimations.Position = new Vector2(96, 96);
                playerIdleAnimations.IsLooping = true;
                playerIdleAnimations.FramesPerSecond = 30;
                #endregion

                #region RUN ANIMATIONS

                // Left
                playerRunAnimations = new SpriteAnimation(run_texture, 10, 1);
                playerRunAnimations.AddAnimation("left", 1, 10, anim.Copy());

                // Switch to Right
                anim.SpriteEffect = SpriteEffects.FlipHorizontally;
                playerRunAnimations.AddAnimation("right", 1, 10, anim.Copy());
                
                playerRunAnimations.Position = new Vector2(96, 96);
                playerRunAnimations.IsLooping = true;
                playerRunAnimations.FramesPerSecond = 30;

                #endregion

                #region JUMP ANIMATIONS
                playerJumpAnimations = new SpriteAnimation(run_texture, 11, 1);
                playerJumpAnimations.AddAnimation("jump", 1, 11, anim.Copy());
                playerJumpAnimations.IsLooping = false;
                #endregion

                #region INITIALIZE START ANIMATIONS
                playerIdleAnimations.Animation = "idle";
                playerRunAnimations.Animation = "idle";
                playerJumpAnimations.Animation = "jump";
                #endregion

            }
            catch (Exception ex) 
            {
                Debug.dd("Load Player Content", ex.Message);
            }
        }
        #endregion

        /// <summary>
        /// Reset Player position
        /// </summary>
        /// <param name="position"></param>
        private void Reset(Vector2 position)
        {
            Position = position;
            Velocity = Vector2.Zero;
            isAlive = true;
            playerRunAnimations.Animation = "idle";
            playerIdleAnimations.Animation = "idle";
        }

        /// <summary>
        /// Handles input, performs physics, and animates the player sprite.
        /// </summary>
        public void Update(GameTime gameTime)
        {
            GetInput();

            ApplyPhysics(gameTime);

            if (IsAlive && IsOnGround)
            {
                if (Math.Abs(Velocity.X) - 0.02f > 0)
                {
                    playerRunAnimations.Animation = "left";
                }
                else
                {
                    playerRunAnimations.Animation = "idle";
                    playerIdleAnimations.Animation = "idle";
                }
            }

            // Clear input.
            movement = 0.0f;
            isJumping = false;
        }

        /// <summary>
        /// 
        /// </summary>
        private void GetInput()
        {
            GamePadState gamePadState = GamePad.GetState(PlayerIndex.One);
            KeyboardState keyboardState = Keyboard.GetState();

            // Get analog horizontal movement.
            movement = gamePadState.ThumbSticks.Left.X * MoveStickScale;
            // Ignore small movements to prevent running in place.
            if (Math.Abs(movement) < 0.5f)
                movement = 0.0f;

            // If any digital horizontal movement input is found, override the analog movement.
            if (gamePadState.IsButtonDown(Buttons.DPadLeft) ||
                keyboardState.IsKeyDown(Keys.Left) ||
                keyboardState.IsKeyDown(Keys.A))
            {
                movement = -1.0f;
            }
            else if (gamePadState.IsButtonDown(Buttons.DPadRight) ||
                   keyboardState.IsKeyDown(Keys.Right) ||
                   keyboardState.IsKeyDown(Keys.D))
            {
                movement = 1.0f;
            }

            // Check if the player wants to jump.
            isJumping =
                gamePadState.IsButtonDown(JumpButton) ||
                keyboardState.IsKeyDown(Keys.Space) ||
                keyboardState.IsKeyDown(Keys.Up) ||
                keyboardState.IsKeyDown(Keys.W);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameTime"></param>
        private void ApplyPhysics(GameTime gameTime)
        {
            throw new NotImplementedException();
        }


    }
}
